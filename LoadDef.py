#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#
from ast import literal_eval as leval
from collections import OrderedDict
import Tkinter as tk
import os
import ttk
import sdl2

"""Load standard definitions in 'global' variables."""

class Language_App(tk.Tk):
    """Class for a TK application to choose a language."""

    def __init__(self, languages):
        """Return a new Language_App."""
        tk.Tk.__init__(self)
        self.resizable(True,False)
        self.minsize(350,100)
        self.maxsize(350,300) # Each language button uses 25 height starting from 100 I.E. 200 will allows 4 buttons.
        self.languages = languages
        self.title("JoyConfig4SC Language Selection.")

        # Create label.
        info_label = ttk.Label(self, text="""
        Welcome to JoyConfig4SC
        A joystick configurator for Star Citizen.

        Please, choose your language below.
        """, justify='center')
        info_label.pack(side="top")

        # Create menu row frame.
        menu_frame = ttk.Frame(self)
        menu_frame.pack(side="bottom", fill="x", expand=False)

        # Create menu buttons.
        for language_name, language_dir in sorted(self.languages.items()):
            comm = lambda dir=language_dir: self._select(dir)
            ttk.Button(menu_frame, text=language_name, command=comm).pack(side="top", fill="both", expand=True)

        # Bind "Window Closed" event.
        self.protocol("WM_DELETE_WINDOW", self.kill)

    def start(self):
        """Create the Application window and run the Tk mainloop."""
        self.mainloop()

    def kill(self):
        """Stop the Application."""
        self.destroy()

    def _select(self, selection):
        """Stop the Application."""
        global LANGUAGE
        LANGUAGE = selection
        self.kill()

def nestedDef2OD(nested):
    """Recursively generate a tree of OrderedDicts."""
    top = OrderedDict(nested)
    for key, value in top.items():
        if type(value) == list:
            top[key] = nestedDef2OD(value)
    return top

# Load standard definitions dicts.
with open(os.path.join("data", "Axes.dict"),"r") as textualdict:
    AXESDICT    = leval(textualdict.read())
with open(os.path.join("data", "XBox.dict"),"r") as textualdict:
    XBOXDICT    = leval(textualdict.read())
with open(os.path.join("data", "Languages.dict"),"r") as textualdict:
    LANGUAGES   = leval(textualdict.read())
with open(os.path.join("data", "Modes.dict"),"r") as textualdict:
    ACTIONMODES = leval(textualdict.read())
with open(os.path.join("data", "Actions.dict"),"r") as textualdict:
    ACTIONMAPD  = leval(textualdict.read())
    ACTIONMAP   = nestedDef2OD(ACTIONMAPD)
EVENTDICT = { sdl2.SDL_FIRSTEVENT               : 'do not remove (unused)',
              sdl2.SDL_QUIT                     : 'user-requested quit; see Remarks for details',
              sdl2.SDL_APP_TERMINATING          : 'OS is terminating the application',
              sdl2.SDL_APP_LOWMEMORY            : 'OS is low on memory; free some',
              sdl2.SDL_APP_WILLENTERBACKGROUND  : 'application is entering background',
              sdl2.SDL_APP_DIDENTERBACKGROUND   : 'application entered background',
              sdl2.SDL_APP_WILLENTERFOREGROUND  : 'application is entering foreground',
              sdl2.SDL_APP_DIDENTERFOREGROUND   : 'application entered foreground',
              sdl2.SDL_WINDOWEVENT              : 'window state change',
              sdl2.SDL_SYSWMEVENT               : 'system specific event',
              sdl2.SDL_KEYDOWN                  : 'key pressed',
              sdl2.SDL_KEYUP                    : 'key released',
              sdl2.SDL_TEXTEDITING              : 'keyboard text editing (composition)',
              sdl2.SDL_TEXTINPUT                : 'keyboard text input',
              sdl2.SDL_MOUSEMOTION              : 'mouse moved',
              sdl2.SDL_MOUSEBUTTONDOWN          : 'mouse button pressed',
              sdl2.SDL_MOUSEBUTTONUP            : 'mouse button released',
              sdl2.SDL_MOUSEWHEEL               : 'mouse wheel motion',
              sdl2.SDL_JOYAXISMOTION            : 'joystick axis motion',
              sdl2.SDL_JOYBALLMOTION            : 'joystick trackball motion',
              sdl2.SDL_JOYHATMOTION             : 'joystick hat position change',
              sdl2.SDL_JOYBUTTONDOWN            : 'joystick button pressed',
              sdl2.SDL_JOYBUTTONUP              : 'joystick button released',
              sdl2.SDL_JOYDEVICEADDED           : 'joystick connected',
              sdl2.SDL_JOYDEVICEREMOVED         : 'joystick disconnected',
              sdl2.SDL_CONTROLLERAXISMOTION     : 'controller axis motion',
              sdl2.SDL_CONTROLLERBUTTONDOWN     : 'controller button pressed',
              sdl2.SDL_CONTROLLERBUTTONUP       : 'controller button released',
              sdl2.SDL_CONTROLLERDEVICEADDED    : 'controller connected',
              sdl2.SDL_CONTROLLERDEVICEREMOVED  : 'controller disconnected',
              sdl2.SDL_CONTROLLERDEVICEREMAPPED : 'controller mapping updated',
              sdl2.SDL_FINGERDOWN               : 'user has touched input device',
              sdl2.SDL_FINGERUP                 : 'user stopped touching input device',
              sdl2.SDL_FINGERMOTION             : 'user is dragging finger on input device',
              sdl2.SDL_DOLLARGESTURE            : 'SDL_DOLLARGESTURE',
              sdl2.SDL_DOLLARRECORD             : 'SDL_DOLLARRECORD',
              sdl2.SDL_MULTIGESTURE             : 'SDL_MULTIGESTURE',
              sdl2.SDL_CLIPBOARDUPDATE          : 'the clipboard changed',
              sdl2.SDL_DROPFILE                 : 'the system requests a file open',
              sdl2.SDL_RENDER_TARGETS_RESET     : 'the render targets have been reset and their contents need to be updated (>= SDL 2.0.2)',
              sdl2.SDL_RENDER_DEVICE_RESET      : 'the device has been reset and all textures need to be recreated (>= SDL 2.0.4)',
              sdl2.SDL_USEREVENT                : 'a user-specified event',
              sdl2.SDL_LASTEVENT                : 'only for bounding internal arrays'}

# Launch Language selection App.
LANGUAGE = None
tkapp = Language_App(LANGUAGES)
tkapp.mainloop()

if LANGUAGE:
    # Load selected language definitions dicts.
    with open(os.path.join("data", LANGUAGE, "Descriptions.dict"),"r") as textualdict:
        ACTIONDESCS = leval(textualdict.read())
    with open(os.path.join("data", LANGUAGE, "Words.dict"),"r") as textualdict:
        WORDSDICT   = leval(textualdict.read())
        globals().update(WORDSDICT)
    with open(os.path.join("data", LANGUAGE, "Texts.dict"),"r") as textualdict:
        TEXTSDICT   = leval(textualdict.read())
        globals().update(TEXTSDICT)

if __name__ == "__main__":
    print LANGUAGE
