"multiplayer"
        respawn:                             x                   Respawn.
        retry:                               x                   Retry.
        ready:                               x                   Start the race.

"singleplayer"
        retry:                               x                   Retry.
        ready:                               x                   Start the race.

"spaceship_general"
        v_exit:                              f                   Leave your actual position/cabin.
        v_eject:                             jsx_reserved        Ejects from your ship.
        v_self_destruct:                     ralt+backspace      Self ship destruction, keep pressed for a while.
        v_toggle_cabin_lights:               jsx_reserved        Turns on/off cabin lights.
        v_toggle_running_lights:             jsx_reserved        Turns on/off external lights.
        v_power_focus_group_1:               1                   Increase potency distribution towards group 1 systems.
        v_power_focus_group_2:               2                   Increase potency distribution towards group 2 systems.
        v_power_focus_group_3:               3                   Increase potency distribution towards group 3 systems.
        v_power_reset_focus:                 4                   Balance potency distribution towards all groups.
        v_toggle_landing_gear:                                   Deploy/Retracts the landing gear.

"spaceship_view"
        v_view_pitch_up:                     js1_hat1_up         Moves line of sight up on free view mode.
        v_view_pitch_down:                   js1_hat1_down       Moves line of sight down on free view mode.
        v_view_pitch:                        js1_y               Controls the vertical line of sight on free view mode.
        v_view_yaw_left:                     js1_hat1_left       Moves line of sight left on free view mode.
        v_view_yaw_right:                    js1_hat1_right      Moves line of sight right on free view mode.
        v_view_yaw:                          js1_x               Controls the horizontal line of sight on free view mode.
        v_view_pitch_mouse:                  maxis_y             Controls the vertical line of sight on free view mode with mouse.
        v_view_yaw_mouse:                    maxis_x             Controls the horizontal line of sight on free view mode with mouse.
        v_view_pitch_absolute:               HMD_Pitch           Controls the vertical line of sight. Supposed for head tracking.
        v_view_yaw_absolute:                 HMD_Yaw             Controls the horizontal line of sight. Supposed for head tracking.
        v_view_roll_absolute:                HMD_Roll            Controls the rolling of line of sight. Supposed for head tracking.
        v_view_cycle_fwd:                    js1_button14        Cycles through different cameras.
        v_view_cycle_headlook_mode:          js1_button5         Turns on/off free view mode.
        v_view_interact:                     f                   Use pointed item.
        v_view_dynamic_focus:                maxis_z             Zoom level axis for mouse.
        v_view_mode:                         jsx_reserved        Changes orbital view mode, external camera only.
        v_view_zoom_in:                      js1_hat1_up         Approaches camera to ship, external camera only.
        v_view_zoom_out:                     js1_hat1_down       Retreats camera from ship, external camera only.
        v_view_dynamic_focus_in:             jsx_reserved        Approaches camera to cockpit.
        v_view_dynamic_focus_out:            jsx_reserved        Retreats camera from cockpit.
        v_view_look_behind:                                      Shows rear view while pressed.

"spaceship_movement"
        v_pitch_up:                          jsx_reserved        Rotates the ship upwards.
        v_pitch_down:                        jsx_reserved        Rotates the ship downwards.
        v_pitch:                             js1_y               Pitch control.
        v_yaw_left:                          jsx_reserved        Horizontally rotates the ship to the left.
        v_yaw_right:                         jsx_reserved        Horizontally rotates the ship to the right.
        v_yaw:                               js1_x               Yaw control.
        v_roll_left:                         jsx_reserved        Longitudinally rotates the ship to the left.
        v_roll_right:                        jsx_reserved        Longitudinally rotates the ship to the right.
        v_roll:                              js1_rotz            Roll control.
        v_strafe_up:                         js1_button20        Pushes the ship upwards.
        v_strafe_down:                       js1_button22        Pushes the ship downwards.
        v_strafe_vertical:                   js1_rotx            Controls lift.
        v_strafe_left:                       js1_button23        Pushes the ship to the left.
        v_strafe_right:                      js1_button21        Pushes the ship to the rigth.
        v_strafe_lateral:                    js1_roty            Controls lateral strafe.
        v_strafe_forward:                    jsx_reserved        Pushes the ship forwards.
        v_strafe_back:                       jsx_reserved        Pushes the ship backwards.
        v_strafe_longitudinal:               jsx_reserved        Controls longitudinal strafe.
        v_brake:                             jsx_reserved        Forces reduction of speed while pressed.
        v_decoupled_pitch_up:                jsx_reserved        Decoupled flight: Rotates the ship upwards.
        v_decoupled_pitch_down:              jsx_reserved        Decoupled flight: Rotates the ship downwards.
        v_decoupled_pitch:                   js1_y               Decoupled flight: Pitch control.
        v_decoupled_yaw_left:                jsx_reserved        Decoupled flight: Horizontally rotates the ship to the left.
        v_decoupled_yaw_right:               jsx_reserved        Decoupled flight: Horizontally rotates the ship to the right.
        v_decoupled_yaw:                     js1_x               Decoupled flight: Yaw control.
        v_decoupled_roll_left:               jsx_reserved        Decoupled flight: Longitudinally rotates the ship to the left.
        v_decoupled_roll_right:              jsx_reserved        Decoupled flight: Longitudinally rotates the ship to the right.
        v_decoupled_roll:                    js1_rotz            Decoupled flight: Roll control.
        v_decoupled_strafe_up:               js1_button20        Decoupled flight: Pushes the ship upwards.
        v_decoupled_strafe_down:             js1_button22        Decoupled flight: Pushes the ship downwards.
        v_decoupled_strafe_vertical:         js1_rotx            Decoupled flight: Controls lift.
        v_decoupled_strafe_left:             js1_button23        Decoupled flight: Pushes the ship to the left.
        v_decoupled_strafe_right:            js1_button21        Decoupled flight: Pushes the ship to the rigth.
        v_decoupled_strafe_lateral:          js1_roty            Decoupled flight: Controls lateral strafe.
        v_decoupled_strafe_forward:          jsx_reserved        Decoupled flight: Pushes the ship forwards.
        v_decoupled_strafe_back:             jsx_reserved        Decoupled flight: Pushes the ship backwards.
        v_decoupled_strafe_longitudinal:     js1_z               Decoupled flight: Controls longitudinal strafe.
        v_decoupled_brake:                   jsx_reserved        Decoupled flight: Forces reduction of speed while pressed.
        v_pitch_mouse:                       jsx_reserved        Mouse Pitch control.
        v_yaw_mouse:                         jsx_reserved        Mouse Yaw control.
        v_toggle_relative_mouse_mode:        lctrl+c             Locks/Unlocks mouse aim sight.
        v_throttle_up:                       jsx_reserved        Accelerate.
        v_throttle_down:                     jsx_reserved        Decelerate.
        v_throttle_abs:                      js1_throttlez       Throttle control.
        v_throttle_rel:                      jsx_reserved        Not sure.
        v_target_match_vel:                  jsx_reserved        Attempts to match target velocity.
        v_throttle_toggle_minmax:            jsx_reserved        Alternates 100%/0% thrust power.
        v_throttle_zero:                     jsx_reserved        Sets 0% thrust power, double tapped.
        v_throttle_100:                      jsx_reserved        Sets 100% thrust power, double tapped.
        v_ifcs_toggle_vector_decoupling:     js1_button8         Switches between Normal(Coupled)/Newtonian(DeCoupled) flight models.
        v_ifcs_toggle_safeties:              jsx_reserved        Alternates security controls configuration.
        v_ifcs_toggle_gforce_safety:         js1_button10        Switches on/off G forces security control.
        v_ifcs_toggle_comstab:               js1_button9         Switches on/off stabilizers security control.
        v_afterburner:                       js1_button7         Activates afterburner while pressed.

"spaceship_targeting"
        v_aim_pitch_up:                      js1_hat1_up         Moves line of sight up.
        v_aim_pitch_down:                    js1_hat1_down       Moves line of sight down.
        v_aim_pitch:                         jsx_reserved        Controls the vertical line of sight.
        v_aim_yaw_left:                      js1_hat1_left       Moves line of sight left.
        v_aim_yaw_right:                     js1_hat1_right      Moves line of sight right.
        v_aim_yaw:                           jsx_reserved        Controls the horizontal line of sight.
        v_aim_pitch_mouse:                   maxis_y             Controls the vertical line of sight with mouse.
        v_aim_yaw_mouse:                     maxis_x             Controls the horizontal line of sight with mouse.
        v_aim_snap:                          jsx_reserved        Centers the view.
        v_target_nearest_hostile:            jsx_reserved        Targets nearest enemy ship.
        v_target_reticle_focus:              js1_button18        Targets the ship by the aim sigth.
        v_target_cycle_all_fwd:              jsx_reserved        Targets next ship.
        v_target_cycle_all_back:             jsx_reserved        Targets previous ship.
        v_target_cycle_friendly_fwd:         jsx_reserved        Targets next friendly ship.
        v_target_cycle_friendly_back:        jsx_reserved        Targets previous friendly ship.
        v_target_cycle_hostile_fwd:          js1_button17        Targets next enemy ship.
        v_target_cycle_hostile_back:         js1_button19        Targets previous enemy ship.
        v_target_cycle_subsystem_fwd:        jsx_reserved        Targets next subsystem for actual target.
        v_target_cycle_subsystem_back:       jsx_reserved        Targets previous subsystem for actual target.
        v_target_cycle_pinned_fwd:                               Targets next pinned target.
        v_target_cycle_pinned_back:                              Targets previous pinned target.
        v_target_toggle_pinned_focused:      js1_button16        Remembers/Forgets target focus for actual target.
        v_target_missile_lock_focused:       js1_button2         Locks missiles on actual target.
        v_couple_aim_to_move:                jsx_reserved        Look Ahead Mode.
        v_toggle_mouse_aim_only:             rshift              HOMAS Mode.
        v_toggle_weapon_gimbal_lock:         jsx_reserved        Gimbal Lock Mode.
        v_target_head_tracking:              js1_button6         Target Focus Mode.

"spaceship_turret"
        v_aim_yaw:                           js1_x               Controls the horizontal line of sight.
        v_aim_yaw_left:                      js1_hat1_left       Moves line of sight left.
        v_aim_yaw_right:                     js1_hat1_right      Moves line of sight right.
        v_aim_pitch:                         js1_y               Controls the vertical line of sight.
        v_aim_pitch_up:                      js1_hat1_up         Moves line of sight up.
        v_aim_pitch_down:                    js1_hat1_down       Moves line of sight down.
        v_aim_yaw_mouse:                     maxis_x             Controls the horizontal line of sight with mouse.
        v_aim_pitch_mouse:                   maxis_y             Controls the vertical line of sight with mouse.
        v_aim_snap:                          jsx_reserved        Centers the view.
        v_target_nearest_hostile:            jsx_reserved        Targets nearest enemy ship.
        v_target_reticle_focus:              js1_button18        Targets the ship by the aim sigth.
        v_target_cycle_all_fwd:              jsx_reserved        Targets next ship.
        v_target_cycle_all_back:             jsx_reserved        Targets previous ship.
        v_target_cycle_friendly_fwd:         jsx_reserved        Targets next friendly ship.
        v_target_cycle_friendly_back:        jsx_reserved        Targets previous friendly ship.
        v_target_cycle_hostile_fwd:          js1_button17        Targets next enemy ship.
        v_target_cycle_hostile_back:         js1_button19        Targets previous enemy ship.
        v_target_cycle_subsystem_fwd:        jsx_reserved        Targets next subsystem for actual target.
        v_target_cycle_subsystem_back:       jsx_reserved        Targets previous subsystem for actual target.
        v_target_missile_lock_focused:       js1_button2         Locks missiles on actual target.
        v_target_toggle_pinned_focused:      js1_button16        Remembers/Forgets target focus for actual target.

"spaceship_weapons"
        v_attack1_group1:                    js1_button1         Fires group 1.
        v_attack1_group2:                    js1_button15        Fires group 2.
        v_attack1_group3:                    js1_button4         Fires group 3.
        v_attack1_group4:                    jsx_reserved        Fires group 4. Not recomended as it uses to be missiles.

"spaceship_missiles"
        v_weapon_launch_missile:             js1_button2         Launches missile, requires a locked target.

"spaceship_defensive"
        v_weapon_launch_countermeasure:      js1_button30        Launches selected countermeasure.
        v_weapon_cycle_countermeasure_fwd:   js1_button31        Cycles through different countermeasures (Chaff/Flare).
        v_shield_raise_level_forward:        js1_button33        Increases bow shields.
        v_shield_raise_level_back:           js1_button34        Increases stern shields.
        v_shield_raise_level_left:           np_4                Increases port shields.
        v_shield_raise_level_right:          np_6                Increases starboard shields.
        v_shield_raise_level_up:             np_8                Increases upper shields.
        v_shield_raise_level_down:           np_2                Increases lower shields.
        v_shield_reset_level:                js1_button32        Sets a balanced shield configuration.

"spaceship_auto_weapons"
        v_weapon_toggle_ai:                  slash               Not sure.

"spaceship_radar"
        v_radar_cycle_zoom_fwd:              js1_button12        Cycles through radar ranges.

"spaceship_hud"
        v_hud_cycle_mode_fwd:                jsx_reserved        Changes to next left hud panel.
        v_hud_cycle_mode_back:               jsx_reserved        Changes to previous left hud panel.
        v_hud_open_tab1:                     jsx_reserved        Switches to left hud *General* panel.
        v_hud_open_tab2:                     js1_button24        Switches to left hud *Weapons* panel.
        v_hud_open_tab3:                     js1_button25        Switches to left hud *Power* panel.
        v_hud_open_tab4:                     js1_button26        Switches to left hud *Shields* panel.
        v_hud_open_scoreboard:               js1_button13        Open/Close scoreboard panel.
        v_hud_toggle_maximised:              f11                 Not sure.
        v_hud_toggle_cursor_input:           tab                 Activates interact cursor while pressed.
        v_hud_interact_toggle:               js1_button3         Activate/Deactivate interact cursor.(toggle)
        v_hud_confirm:                       js1_button1         Confirm with interact cursor.
        v_hud_cancel:                        jsx_reserved        Cancel with interact cursor.
        v_hud_left_panel_up:                 js1_button16        Moves up on left hud panel in interact mode.
        v_hud_left_panel_down:               js1_button18        Moves down on left hud panel in interact mode.
        v_hud_left_panel_left:               js1_button19        Moves left on left hud panel in interact mode.
        v_hud_left_panel_right:              js1_button17        Moves right on left hud panel in interact mode.
        v_comm_open_chat:                    backslash           Open/Close Chat input line.
        v_comm_show_chat:                    equals              Show/Hide chat messages.

"zero_gravity_general"
        z_brake:                             js1_button32        Brake on Zero-G.
        z_roll:                              js1_x               Roll on Zero-G.


