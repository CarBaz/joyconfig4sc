#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#
import Tkinter as tk
import ttk

lineal_data = [0,0,0.1,0.1,0.2,0.2,0.3,0.3,0.4,0.4,0.5,0.5,0.6,0.6,0.7,0.7,0.8,0.8,0.9,0.9,1,1]
class PlotGrid(ttk.Frame):
    """A grid for plotting curves."""

    def __init__(self, parent, *args, **kw):
        """."""
        ttk.Frame.__init__(self, parent)
        kw['height'] = kw['height'] + 25
        self.canvas = tk.Canvas(self, *args, **kw)
        self.canvas.pack(side="top")
        self.button_frame = ttk.Frame(self)
        self.button_frame.pack(side="top", fill="x", expand=True)
        self.grid_density = 20
        self.origin = 10
        self.ending = kw['width']
        self.size   = self.ending - self.origin
        self.smooth = True

        # Draw X axis.
        self.canvas.create_line(self.origin, self.ending, self.ending, self.ending, width=2)

        # Draw Y axis.
        self.canvas.create_line(self.origin, self.ending, self.origin, self.origin, width=2)

        # Draw Grid.
        step = self.size/float(self.grid_density)
        for i in range(self.grid_density+1):
            displacement = self.origin+(i*step)
            # Draw Horizontal lines.
            self.canvas.create_line(displacement, self.origin, displacement, self.ending)
            # Draw Vertical lines.
            self.canvas.create_line(self.origin, displacement, self.ending, displacement)

        # Draw Main Diagonal line.
        self.canvas.create_line(self.origin, self.ending, self.ending, self.origin, fill='red')

        mid = step*self.grid_density/2+self.origin
        # Draw Middle Horizontal line.
        self.canvas.create_line(self.origin, mid, self.ending, mid, fill='orange')

        # Draw Middle Vertical line.
        self.canvas.create_line(mid, self.origin, mid, self.ending, fill='orange')

        tips=self.ending/3
        # Draw Legend.
        self.canvas.create_line(self.origin + 5, self.ending + 15, self.origin + 35, self.ending + 15, fill='Yellow', width=2)
        self.canvas.create_text(self.origin + 40,  self.ending + 15, text='Lineal^Exp', anchor='w')
        self.canvas.create_line(self.origin + 5 + tips, self.ending + 15, self.origin + 35 + tips, self.ending + 15, fill='blue',   width=2)
        self.canvas.create_text(self.origin + 40 + tips, self.ending + 15, text='Curve', anchor='w')
        self.canvas.create_line(self.origin + 5 + tips*2, self.ending + 15, self.origin + 35 + tips*2, self.ending + 15, fill='green',  width=2)
        self.canvas.create_text(self.origin + 40 + tips*2, self.ending + 15, text='Curve^Exp', anchor='w')

        # Create controls.
        self.smooth_var = tk.StringVar()
        self.smooth_var.set("UnSmooth Curves")
        bt_smooth = lambda : self._switch_smooth()
        # Create the "Soften" button.
        ttk.Button(self.button_frame, textvariable=self.smooth_var , command=bt_smooth).pack(side="top", fill="both", expand=True)
        ttk.Label(self.button_frame, text="Smoothing is only a visual option, game takes unsmoothed values").pack(side="top")

        # Draw Curves.
        self.lineal_curve = self.canvas.create_line([self.origin, self.ending, self.ending, self.origin],
                                                     width=2, smooth=self.smooth, capstyle="round", fill='yellow')
        self.point_curve  = self.canvas.create_line([self.origin, self.ending, self.ending, self.origin],
                                                     width=2, smooth=self.smooth, capstyle="round", fill='blue')
        self.exp_curve    = self.canvas.create_line([self.origin, self.ending, self.ending, self.origin],
                                                     width=2, smooth=self.smooth, capstyle="round", fill='green')

    def _switch_smooth(self):
        self.smooth = not self.smooth
        if self.smooth:
            self.smooth_var.set("UnSmooth Curves")
        else:
            self.smooth_var.set("Smooth Curves")
        self.canvas.itemconfigure(self.lineal_curve, smooth=self.smooth)
        self.canvas.itemconfigure(self.point_curve, smooth=self.smooth)
        self.canvas.itemconfigure(self.exp_curve, smooth=self.smooth)

    def _scale_datas(self, datas):
        """Scale and displace datas to match grid."""
        X_axis = True
        scaled_datas = []
        for data in datas:
            if X_axis:
                displaced = (data * self.size) + self.origin
            else:
                displaced = self.ending - (data * self.size)
            scaled_datas.append(displaced)
            X_axis = not X_axis
        return scaled_datas

    def _power_datas(self, datas, exp):
        """Scale Y datas to exp."""
        X_axis = True
        scaled_datas = []
        for data in datas:
            if X_axis:
                displaced = data
            else:
                displaced = data**exp
            scaled_datas.append(displaced)
            X_axis = not X_axis
        return scaled_datas

    def _invert_datas(self, datas, invert):
        """Invert Y datas relative to X."""
        if invert:
            X_axis = True
            X_datas = []
            Y_datas = []
            inverted_datas = []
            for data in datas:
                if X_axis:
                    X_datas.append(data)
                else:
                    Y_datas.append(data)
                X_axis = not X_axis
            Y_datas.reverse()
            for i in range(len(X_datas)):
                    inverted_datas.append(X_datas[i])
                    inverted_datas.append(Y_datas[i])
            return inverted_datas
        else:
            return datas

    def _sense_datas(self, datas, sens):
        """Increase data values on sense."""
        X_axis = True
        scaled_datas = []
        for data in datas:
            if X_axis:
                displaced = data
            else:
                displaced = data*sens
                if displaced > 1:
                    displaced = 1
            scaled_datas.append(displaced)
            X_axis = not X_axis
        return scaled_datas

    def redrawCurve(self, datas, invert, sens, exp, verbose=False):
        """."""
        X_axis = True
        lineal_curve_data = self._scale_datas(self._power_datas(self._invert_datas(self._sense_datas(lineal_data, sens), invert), exp))
        point_curve_data  = self._scale_datas(self._invert_datas(datas, invert))
        exp_curve_data    = self._scale_datas(self._power_datas(self._invert_datas(self._sense_datas(datas, sens), invert), exp))

        # Draw exponential
        self.canvas.coords(self.lineal_curve, *lineal_curve_data)
        # Draw base curve
        self.canvas.coords(self.point_curve,  *point_curve_data)
        # Draw exponented curve
        self.canvas.coords(self.exp_curve,    *exp_curve_data)
        if verbose:
            print 'redraw'

def MAIN():
    root = tk.Tk()
    root.title('Test PlotGrid.')
    grid = PlotGrid(root, width=510, height=510, bd=3, relief='ridge')
    grid.pack()
    ttk.Button(root, text='Quit', command=root.quit).pack()
    root.after(2000, grid.redrawCurve, *([],0,2.,1.,1.))
    root.after(8000, grid.redrawCurve, *([],1,1.,1.,1.))
    root.after(12000, grid.redrawCurve, *([],0,1.,2.,1.))
    root.after(16000, grid.redrawCurve, *([],1,1.,2.,1.))
    root.mainloop()

if __name__ == "__main__":
    MAIN()

