#
# Based on: http://tkinter.unpythonic.net/wiki/VerticalScrolledFrame
#
# Modified by: Carlos Bazaga
#   Exposed 'canvas' attribute to allow for direct scrolling.
#
import Tkinter as tk
import ttk

class HorizontalScrolledFrame(ttk.Frame):
    """
    A pure Tkinter scrollable frame.

    Use the 'interior' attribute to place widgets inside the scrollable frame.
    Construct and pack/place/grid normally.
    This frame only allows horizontal scrolling.

    Mods:
        Use the 'canvas' attribute to handle direct scroll.
            IE:
                    # If the required height of the 'interior' is greater
                    # than the allowed space in 'canvas' scrolling is required.

                    if my_hscrolledframe.interior.winfo_reqwidth() > my_hscrolledframe.canvas.winfo_width():
                        my_hscrolledframe.canvas.xview_scroll(-1*(event.delta/120), "units")
    """

    def __init__(self, parent, *args, **kw):
        ttk.Frame.__init__(self, parent, *args, **kw)

        # create a canvas object and a horizontal scrollbar for scrolling it
        hscrollbar = ttk.Scrollbar(self, orient="horizontal")
        hscrollbar.pack(fill="x", side="top", expand=False)
        self.canvas = canvas = tk.Canvas(self, bd=0, highlightthickness=0, xscrollcommand=hscrollbar.set)
        self.canvas.pack(side="left", fill="both", expand=True)
        hscrollbar.config(command=self.canvas.xview)

        # reset the view
        canvas.xview_moveto(0)
        canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = interior = ttk.Frame(canvas)
        interior_id = canvas.create_window(0, 0, window=interior, anchor="nw")

        # track changes to the canvas and frame height and sync them,
        # also updating the scrollbar
        def _configure_interior(event):
            # update the scrollbars to match the size of the inner frame
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            if interior.winfo_reqheight() != canvas.winfo_height():
                # update the canvas's height to fit the inner frame
                canvas.config(height=interior.winfo_reqheight())
        interior.bind('<Configure>', _configure_interior)

        def _configure_canvas(event):
            if interior.winfo_reqheight() != canvas.winfo_height():
                # update the inner frame's height to fill the canvas
                canvas.itemconfigure(interior_id, height=canvas.winfo_height())
        canvas.bind('<Configure>', _configure_canvas)