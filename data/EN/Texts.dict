﻿# -*- coding: utf-8 -*-
#ENGLISH
{
'HEADWARN' : """There is no need to use a header to load layouts via console,
This is just for those pretending load via in game menu, which is discouraged.""",

'HOMETEXT' : """
Welcome to JoyConfig4SC.

Version: $$VERSION$$

Year: $$YEAR$$

Written by: Carlos Bazaga.

Licensed under BSD 2-Clause.

Thanks to all contributors,
 testers and translators.

This program is independent of
 Cloud Imperium Games and is not
 representative of their work.

It is provided to the community to
 help writing controller .xml
 configuration files.

Star Citizen names and content are
 Copyright © Cloud Imperium Games.

""",

'CURVEHELP' : """                   How to use the curve definition tool:

First add different control points with the below form.

    Enter the X position in the field and hit "Add point" button.
    Same process to remove a slider.

    Range goes from 0 to 1, both not included. (So 0.001 to 0.999)

    Use the "Next" button to change visualization of curves.

Once added the required points use each slider to set the response.

    Sliders can be dragged up and down, also you can click the slider rail
    up or down to displace the slider in 0.001 steps.

    For more detailed info on how a response curve works and how the game
    uses it, take a look at SkyZone's "Dealing with nonlinear multi-point curves tutorial"

        https://dl.dropboxusercontent.com/u/13355037/Multi%20point%20curves-1.pdf

In the graph you can see three curves:
    (NOTE: Ony first cuadrant represented)

    -Yellow:    This curve shows the effect of sensitivity and exponent
                      applied on a lineal curve. (the standard diagonal)

    -Blue:       This is the base curve defined by the sliders array.
                      It is not affected by any filter.

    -Green:     This is the base curve affected by the sensitivity
                      and exponent parameters.

                      Both can be set separately for each curve
                      on the upper right fields.

                      This curve is what the game will use.

Under the graph you will find a smooth/unsmooth button (UnSmooth Curves).

    Smoothing is only a visual helper for representation.

    While unknowing exactly which interpolation is used by the game, we must
    assume the game will work like unsmoothed one.
        (Linear interpolation)

Once the final curves you want are set and with their desired
sensitivity and exponent, click the "Set Options" button
to load them into the profile.

You can see them loaded in the "XML Code" tab.

NOTE: Clear buttons will clear the settings for the joystick from the XML,
            but will not reset values on the joystick tabs.""",

'DIHELPT' : """Some example codes to bind functions:    (This list does not pretend to be exhaustive)""",

'DIHELPL' : """    Mouse bindings:    (Only for KBBOUND Actions)

        •maxis_x:\t\tSide movement.
        •maxis_y:\t\tFrontal movement.
        •maxis_z:\t\tMousewheel.
        •mwheel_up:\t\tPush mousewheel.
        •mwheel_down:\tPull mousewheel.
        •mouse1:\t\tMouse button 1.
        •mouse2:\t\tMouse button 2.
        •mouse3:\t\tMouse button 3.
        ...


    Special Joystick Bindings:
      (Those binds are not necessarily recognized by all joysticks)

        •jsx_throttlez:\t\tIn some joysticks an alias for throttle control.
\t\t\tIt could solve the problem of having 100% power
\t\t\tat half trottle stick way. x should be the joy number.
\t\t\tExample: js1_throttlez

        •jsx_throttleW:\tAn alias for "throttle" behaviour for
\t\t\tsome axes. W should be the axis name.

""",

'DIHELPR' : """    Keyboard keys:    (Only for KBBOUND Actions)

        Keys can be bound by "combos" like: ralt+k

        •tab:\tTab key.
        •escape:\tEscape key.
        •insert:\tInsert key.
        •capslock:\tCaps lock key.
        •lalt:\tLeft Alt key.
        •ralt:\tRight Alt key.
        •lctrl:\tLeft Control key.
        •rctrl:\tRight Control key.
        •lshift:\tLeft Shift key.
        •rshift:\tRight Shift key.
        •backspace:\tBackspace key.
        •np_x:\tNumpad key, for example: np_1 or np_divide
        •fx:\t\tFucntion keys, for example: f5
        •x:\t\tLetter or number key, for example: w or 5
        •right:\tRight key.
        •left:\tLeft key.
        •up:\t\tUp key.
        •down:\tDown key.
        ...
""",

'MANUALTEXT' : """
                                         USE TIPS:                                                          @

    Use the "Joysticks Tab" and test each one before starting to capture in the Edit tab.
        This will allow a proper initialization of the joysticks.

    Sometimes Axes of the Joysticks can "vibrate" and make axis capture a pain.
        Setting a big "dead zone" for them to avoid them being captured so easily can help,
        once the configuration process is finalized, remember to restore the dead zones to their previous values.

    Sometimes the event buffer does not clear properly and captures anything automatically.
        Click capture button until it stops to "auto capture" events and the pop-up remains static.

    Save your configuration often.

                                        USE MANUAL:

The application is divided into five main tabs:

Home:

    This is the actual tab, it contains a simple manual of use for the application.

    Includes a "Joystick Reload" button.
        This will attempt to search for recently connected or disconnected joysticks after launch of the tool.
        Disconnecting a joystick while running can be detected but no necessarily its reconnection.
        It would require some tries until it correctly detects the actual devices.
        There is no need to use it if all joysticks were connected before the program was launched.

    ** It's not recommended to plug in or unplug joysticks while running the program. **

Joysticks:

    There you can test each connected joystick, each joystick is represented with its own tab.
        (Pedal devices are considered joysticks as well, so they will be configured the same way.)

    Each tab is divided in four sections:

        Info:    This frame shows the actual in-game assigned ID.
                     This ID it's supposed to be correctly detected by the application.
                     This is the ID Star Citizen will use to identify each device.
                     In case of need it can be manually changed with the field below.

                 Also shows the GUID numbers. That's related to hardware information of the device.

                 There is a last button to switch the axis numbers interpretation.
                    This will change the name asigned to each axis and may solve some errors.

        Buttons: Shows each button number and its actual status. (1: Pressed, 0: Released)

        Axes:    Shows each axis name and its actual value.
                    Stick values range from -100 to 100 being 0 the centered position.
                    Rest of axes ranges from 0 to 100.

        Hats:    Shows each hat number and its actual position.

Edit Layout:

    The editing tab is divided into a set of subtabs, one for each mapping category.

    For each bindable action on each category it is shown his actual binding and a description.

    Just below are three buttons for each action:

        Capture:      Creates a popup window which waits for a joystick event to be captured.
                         Once an event is captured it is show for a short amount of time.
                         In case of a collision a list of actions sharing the binding will pop up.
                         It will be required for the user to actively accept any colliding binding.

        Direct Input: Allows entering the code manually.
                         It will be useful if the joystick event capture does not work properly.
                         Also it can be used to map keyboard bindings and other devices.

        Clear Input:  Resets the binding for the selected action.
                         "jsx_reserved" would be used as default value so it has no effect in-game.

Mapping:

    This tab shows a summary of the actual layout.
        It shows actual bindings and descriptions for each action along its descriptions.
        Can be used to document a shared XML layout.

        To export the text can be selected and copied to a file.
            There is no need to expand the window enough to avoid the line adjustment,
            it will introduce no "new_lines" in the copied text.

.XML Code:

    This tab shows the actual XML code for the configured bindings.

        Can be exported the same way as "Mapping" via a "copy-paste" operation.

    This can also be saved via the bottom buttons.

        Load Layout:    Loads a new layout,
                            The actions to be bound will be those present in the layout.

        Save Layout:    Saves the actual bindings to a file.
                            This will erase the given file and copy the entire .XML code.

Copyright (c) 2014, Carlos Bazaga.
All rights reserved.
""",
}
