#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#
from VerticalScrolledFrame import VerticalScrolledFrame as VSFrame
from HorizontalScrolledFrame import HorizontalScrolledFrame as HSFrame
from ScrolledText import ScrolledText as SText
from PlotGrid import PlotGrid as PGrid
from collections import OrderedDict
from unicodedata import normalize
import tkFileDialog as FDialog
import tkMessageBox as MBox
from itertools import cycle
import Tkinter as tk
import tkFont
import time
import ttk
import sys
import os

# Pretty printing for debug purposes:
from pprint import PrettyPrinter
PPRINT = PrettyPrinter(indent=4, width=140).pprint

# Set environment path var for SDL library.
if os.name != "posix":
    os.environ["PYSDL2_DLL_PATH"] = '{0}\lib'.format(os.path.dirname(os.path.abspath(sys.argv[0])))
import sdl2
import sdl2.ext

# Import language Definitions.
from LoadDef import *

# Codes Definitions.
VERSION = '4.4'
YEAR    = '2015'
NULLINP = " " #"jsx_reserved"
ATTBTTN = sdl2.SDL_JOYBUTTONUP
ATTAXIS = sdl2.SDL_JOYAXISMOTION
ATTHATS = sdl2.SDL_JOYHATMOTION
LOGDATA = False
LOGCONS = False
LOGFILE = 'EventLogs.txt'
ECHOLOD = True

# Class Definitions.
class Mapping:
    """Class representing a mapping for Star Citizen."""

    def __init__(self, actionmaps):
        """Create a new Mapping object for the given actionmaps."""
        self.joy_names  = {}
        self.actionmaps = actionmaps
        self.deviceopts = {}
        self.modifiers  = set()
        self.options    = {}
        self.header     = ""

    def get_input(self, input):
        """Return the list of actions currently binded to given Input."""
        collisions = []
        for actionmap_name, actionmap in self.actionmaps.items():
            for action_name, action_dict in actionmap.items():
                if input in action_dict["input"].values():
                    collisions.append("{0}.{1}".format(actionmap_name, action_name))
        return collisions

    def get_text(self):
        """Return the textual configuration for the actual bindings."""
        txt = ''
        for actionmap_name, actionmap in self.actionmaps.items():
            txt += '"{0}"\n'.format(actionmap_name)
            for action_name, action_dict in actionmap.items():
                inputs = list_uniques(action_dict["input"].values())
                if action_dict["device"] != "unknown":
                    txt += '        {0}:{1} {2}{3}{4}\n'.format(action_name, ' '*(35-len(action_name)), inputs[0],
                                                                ' '*(20-len(inputs[0])), ACTIONDESCS.get(action_name, 'Not Set'))
                for bind in inputs[1:]:
                    txt += '        {0}:{1} {2}{3}{4}\n'.format(action_name, ' '*(35-len(action_name)), bind,
                                                                ' '*(20-len(bind)), ACTIONDESCS.get(action_name, 'Not Set'))
            txt += '\n'
        txt += '\n'
        return txt

    def get_xml(self):
        """Return the configuration .xml for the actual bindings."""
        xml = '<!-- Layout by JoyConfig4SC. -->\n<ActionMaps ignoreVersion="1" >\n'
        if self.header:
            xml += '    <CustomisationUIHeader label="{0}" description="@ui_JoystickDefaultDesc" image="JoystickDefault">\n'.format(self.header)
            xml += '        <Devices>\n'
            xml += '            <keyboard instance="1" />\n'
            xml += '            <joystick instance="1" />\n'
            xml += '            <joystick instance="2" />\n'
            xml += '            <joystick instance="3" />\n'
            xml += '            <joystick instance="4" />\n'
            xml += '        </Devices>\n'
            xml += '    </CustomisationUIHeader>\n'
        if self.modifiers:
            xml += '    <modifiers>\n'
            for modifier in self.modifiers:
                xml += '        <add input="{0}" />\n'.format(modifier)
            xml += '    </modifiers>\n'
        for device, dead_zones in self.deviceopts.items():
            # No deadzones coded for XboxPad.
            if not device.startswith('XInput Controller'):
                x_dz, y_dz, z_dz, rx_dz, ry_dz, rz_dz, s1_dz, s2_dz = dead_zones
                xml += '    <deviceoptions name="{0}">\n'.format(device)
                xml += '        <option input="x" deadzone="{0}"/>\n'.format(x_dz)
                xml += '        <option input="y" deadzone="{0}"/>\n'.format(y_dz)
                xml += '        <option input="z" deadzone="{0}"/>\n'.format(z_dz)
                xml += '        <option input="throttlez" deadzone="{0}"/>\n'.format(z_dz)
                xml += '        <option input="rotx" deadzone="{0}"/>\n'.format(rx_dz)
                xml += '        <option input="roty" deadzone="{0}"/>\n'.format(ry_dz)
                xml += '        <option input="rotz" deadzone="{0}"/>\n'.format(rz_dz)
                xml += '        <option input="slider1" deadzone="{0}"/>\n'.format(s1_dz)
                xml += '        <option input="slider2" deadzone="{0}"/>\n'.format(s2_dz)
                xml += '    </deviceoptions>\n'
        for joy, curves in self.options.items():
            if self.joy_names[joy].startswith('XInput Controller'):
                xml += '    <options type="xboxpad" instance="{0}">\n'.format(self.joy_names[joy][19:])
            else:
                xml += '    <options type="joystick" instance="{0}">\n'.format(joy)
            for curve, options in curves.items():
                invert      = options['invert']
                exponent    = options['exponent']
                sensitivity = options['sense']
                xml += '        <{0} invert="{1}" sensitivity="{2}" exponent="{3}">\n'.format(curve, invert, sensitivity, exponent)
                xml += '            <nonlinearity_curve>\n'
                for Xpoint, Ypoint in sorted(options['data'].items()):
                    xml += '                <point in="{0}" out="{1}"/>\n'.format(Xpoint, Ypoint)
                xml += '            </nonlinearity_curve>\n'
                xml += '        </{0}>\n'.format(curve)
            xml += '    </options>\n'
        for actionmap_name, actionmap in self.actionmaps.items():
            xml += '    <actionmap name="{0}">\n'.format(actionmap_name)
            for action_name, action_dict in actionmap.items():
                xml += '        <action name="{0}">\n'.format(action_name)
                inputs = list_uniques(action_dict["input"].values())
                if action_dict["device"] != "unknown":
                    if inputs[0][:3] == 'xi_':
                        kind = 'xboxpad'
                    elif inputs[0][:2] == 'js':
                        kind = 'joystick'
                    else:
                        kind = action_dict["device"]
                    xml += '            <rebind device="{0}" input="{1}"/>\n'.format(kind, inputs[0])
                for bind in inputs[1:]:
                    if bind[:3] == 'xi_':
                        kind = 'xboxpad'
                    else:
                        kind = 'joystick'
                    xml += '            <addbind device="{0}" input="{1}"/>\n'.format(kind, bind)
                xml += '        </action>\n'
            xml += '    </actionmap>\n'
        xml += '</ActionMaps>\n<!-- /Layout by JoyConfig4SC. -->\n'
        return xml

    def set_modifier_key(self, bind_value):
        """Add a modifier input if not already and return if it was effectively added."""
        if bind_value not in self.modifiers:
            self.modifiers.add(bind_value)
            return True
        else:
            return False

    def del_modifier_key(self, bind_value):
        """Remove a modifier input."""
        self.modifiers.discard(bind_value)

    def set_action_input(self, actionmap, action, input, line):
        """Bind "input" for "action" on "actionmap" and return the list of actions currently binded to given Input."""
        if self.actionmaps.has_key(actionmap):
            if self.actionmaps[actionmap].has_key(action):
                self.actionmaps[actionmap][action]["input"][line] = input
            else:
                print("ERROR: No Action to Set")
        else:
            print("ERROR: No Actionmap to Set")

    def del_action_input(self, actionmap, action, line):
        """Delete the "input" for "action" on "actionmap" and return the Input it had."""
        if self.actionmaps.has_key(actionmap):
            if self.actionmaps[actionmap].has_key(action):
                return self.actionmaps[actionmap][action]["input"].pop(line)
            else:
                print("ERROR: No Action to Set")
        else:
            print("ERROR: No Actionmap to Set")

    def get_action_input(self, actionmap, action):
        """Return the list of bindings for the action."""
        if self.actionmaps.has_key(actionmap):
            if self.actionmaps[actionmap].has_key(action):
                return self.actionmaps[actionmap][action]["input"].values()
            else:
                print("ERROR: No Action to Get")
        else:
            print("ERROR: No Actionmap to Get")

    def set_deviceopt(self, device, x_dz="0.015", y_dz="0.015", z_dz="0.015", rx_dz="0.015", ry_dz="0.015", rz_dz="0.015", s1_dz="0.015", s2_dz="0.015"):
        """Set the given device options."""
        self.deviceopts[device] = (x_dz, y_dz, z_dz, rx_dz, ry_dz, rz_dz, s1_dz, s2_dz)

    def get_deviceopt(self, device):
        """Get the given device options."""
        return self.deviceopts.get(device, (0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015))

    def set_options(self, joy, curves):
        """Set the given axes options."""
        self.options[joy] = curves

    def del_deviceopt(self, device):
        """Unset the deadzones for the given device."""
        self.deviceopts.pop(device, None)

    def del_options(self, instance):
        """Unset the axes options for the given joystick instance."""
        self.options.pop(instance, None)

    def set_modifiers(self, modifiers):
        """Set the modifiers list."""
        self.modifiers = modifiers

    def get_modifiers(self):
        """Return modifiers list."""
        return list(self.modifiers)

    def set_map(self, actionmaps):
        """Set the given actionmaps."""
        if ECHOLOD: print('\nLoading Layout:\n---------------')
        for actionmap_name, actionmap in actionmaps.items():
            if self.actionmaps.has_key(actionmap_name):
                for action_name, action_dict in actionmap.items():
                    if self.actionmaps[actionmap_name].has_key(action_name):
                        if ECHOLOD: print('{0:55}\n\t\t{1:60} ->   {2:60}\n'.format('{0}.{1}:'.format(actionmap_name, action_name),
                                                                              '{0}="{1}"'.format(self.actionmaps[actionmap_name][action_name]['device'],
                                                                                                 ', '.join(self.actionmaps[actionmap_name][action_name]['input'].values())),
                                                                              '{0}="{1}"'.format(action_dict['device'],', '.join(action_dict['input'].values()))))
                        self.actionmaps[actionmap_name][action_name] = action_dict
                    else:
                        if ECHOLOD: print('{0:55}\tACTION NOT EXISTS IN THIS VERSION, IGNORED.'.format('{0}.{1}:'.format(actionmap_name, action_name)))
            else:
                if ECHOLOD: print('{0:55}\tACTIONMAP NOT EXISTS IN THIS VERSION, IGNORED.'.format('{0}:'.format(actionmap_name)))
        if ECHOLOD: print('----------------------------------------------------------------------------------------------------------------------\n')

    def set_dev(self, deviceopts):
        """Set the devices options."""
        self.deviceopts = deviceopts

    def set_opt(self, options):
        """Set the axes options."""
        self.options = options

    def set_joy_name(self, joy, name):
        self.joy_names[joy] = name

    def set_head(self, head):
        """Set the header name."""
        self.header = head

    def del_head(self):
        self.header = ""

class Translator:
    """Implements an "Event" to Star Citizen translator."""

    def __init__(self, axesdict):
        """Return a new translator with the loaded definition."""
        self.axes_dict   = axesdict
        self.axesdicts   = sorted([map for map in self.axes_dict.values()], key=lambda x: x['name'])
        self.joys_dict   = {-1:'x'}
        self.hats_dict   = {sdl2.SDL_HAT_UP:           'up',       sdl2.SDL_HAT_RIGHT:     'right',
                            sdl2.SDL_HAT_DOWN:         'down',     sdl2.SDL_HAT_LEFT:      'left',
                            sdl2.SDL_HAT_CENTERED:     'centered', sdl2.SDL_HAT_LEFTUP:    'up',
                            sdl2.SDL_HAT_RIGHTUP:      'up',       sdl2.SDL_HAT_LEFTDOWN:  'down',
                            sdl2.SDL_HAT_RIGHTDOWN:    'down'}

        self.hats_dict_v = {sdl2.SDL_HAT_UP:           UP,         sdl2.SDL_HAT_RIGHT:     RIGHT,
                            sdl2.SDL_HAT_DOWN:         DOWN,       sdl2.SDL_HAT_LEFT:      LEFT,
                            sdl2.SDL_HAT_CENTERED:     CENTER,     sdl2.SDL_HAT_LEFTUP:    UPLEFT,
                            sdl2.SDL_HAT_RIGHTUP:      UPRIGHT,    sdl2.SDL_HAT_LEFTDOWN:  DOWNLE,
                            sdl2.SDL_HAT_RIGHTDOWN:    DOWNRI}

    def translate_joy(self, joy):
        """Translate joystick number."""
        return '{0}'.format(self.joys_dict[joy])

    def translate_joy_num(self, joy):
        """Translate joystick number."""
        return '{0}'.format(self.joys_dict[joy])

    def translate_button(self, button, joy_name):
        """Translate joystick button."""
        if joy_name.startswith('XInput Controller'):
            return XBOXDICT.get(button + 1, 'button{0}'.format(button + 1))
        else:
            return 'button{0}'.format(button + 1)

    def translate_button_num(self, button):
        """Translate joystick button."""
        return button + 1

    def translate_hat(self, hat):
        """Translate joystick hat position."""
        return 'hat{0}'.format(hat + 1)

    def translate_hat_num(self, hat):
        """Translate joystick hat position."""
        return hat + 1

    def translate_hat_value(self, hat, value):
        """Translate joystick hat position."""
        return 'hat{0}_{1}'.format(hat + 1, self.hats_dict.get(value, value))

    def translate_hat_pos(self, value):
        """Translate joystick hat position."""
        return '{0}'.format(self.hats_dict_v.get(value, value))

    def translate_axis(self, axis, joy_name):
        """Translate joystick axis."""
        if self.axes_dict.has_key(joy_name):
            return self.axes_dict[joy_name][axis]
        elif joy_name.startswith('XInput Controller'):
            return self.axes_dict['Standard XBoxPad'][axis]
        else:
            return self.axes_dict['Standard Direct Input 5'][axis]

    def translate_event(self, event, joy_name):
        """Translate joystick event."""
        if joy_name.startswith('XInput Controller'):
            joyID = 'xi'+self.translate_joy(event.which)
        else:
            joyID = 'js'+self.translate_joy(event.which)
        if   event.type == ATTAXIS:
            return '{0}_{1}'.format(joyID, self.translate_axis(event.axis, joy_name))
        elif event.type == ATTBTTN:
            return '{0}_{1}'.format(joyID, self.translate_button(event.button, joy_name))
        elif event.type == ATTHATS:
            return '{0}_{1}'.format(joyID, self.translate_hat_value(event.hat, event.value))
        else:
            return "jsx_unknown"

    def remap_joy(self, joy_number, joy_trans):
        """Set a new translation for a joystick."""
        self.joys_dict[joy_number] = joy_trans

    def remap_axes(self, joy_name, axes_dict):
        self.axes_dict[joy_name] = axes_dict
        return self.axes_dict[joy_name]['name']

    def get_axes_map(self, joy_name):
        if self.axes_dict.has_key(joy_name):
            return self.axes_dict[joy_name]['name']
        elif joy_name.startswith('XInput Controller'):
            return self.axes_dict['Standard XBoxPad']['name']
        else:
            return self.axes_dict['Standard Direct Input 5']['name']

    def normalize_axis(self, axis, joy_name, value):
        """
        Return a integer normalized value.
            This value will be:
                Between -100 to 100 for sliding axes
                Between 0 to 100 for stick centered axes.
        """
        if self.translate_axis(axis, joy_name) in ['x','y','rotz']:
            return int(value/327.67)
        else:
            return int((value/327.67)+100)/2

class Config_App(tk.Tk):
    """Class for a TK application to map joysticks."""

    def __init__(self, mapping, translator):
        """Return a new Config_App."""
        tk.Tk.__init__(self)
        self.mapping    = mapping
        self.translator = translator
        self.joysticks  = []
        self.ignorelist = set()

        self.title("JoyConfig4SC")

        # Create menu row frame.
        menu_frame = ttk.Frame(self)
        menu_frame.pack(side="bottom", fill="x", expand=False)

        # Create menu buttons.
        ttk.Button(menu_frame, text=LOADBTT, command=self._load).pack(side="left", fill="both", expand=True)
        ttk.Button(menu_frame, text=SAVEBTT, command=self._store).pack(side="left", fill="both", expand=True)
        ttk.Button(menu_frame, text=SAVEMAP, command=self._summ).pack(side="left", fill="both", expand=True)

        # Create Tabs Container.
        self.tabs_pane = ttk.Notebook(self, padding=5)
        self.tabs_pane.pack(side="top", fill="both", expand=True)

        # Create Tabs Frames.
        self.home_frame = ttk.Frame(self.tabs_pane, padding=5)
        self.joys_frame = ttk.Frame(self.tabs_pane, padding=5)
        self.edit_frame = ttk.Frame(self.tabs_pane, padding=5)
        self.vmap_frame = ttk.Frame(self.tabs_pane, padding=5)
        self.vxml_frame = ttk.Frame(self.tabs_pane, padding=5)
        self.head_frame = ttk.Frame(self.tabs_pane, padding=5)

        # Bind Tabs to Frames.
        self.tabs_pane.add(self.home_frame, text=HOMETAB)
        self.tabs_pane.add(self.joys_frame, text=JOYSTAB)
        self.tabs_pane.add(self.edit_frame, text=EDITTAB)
        self.tabs_pane.add(self.vmap_frame, text=MAPTAB )
        self.tabs_pane.add(self.vxml_frame, text=XMLTAB )
        self.tabs_pane.add(self.head_frame, text=HEADTAB )

        # Create Tabs Widgets.
        self.home_hook = ttk.Frame(self.home_frame)
        self.joys_tabs = ttk.Notebook(self.joys_frame)
        self.edit_tabs = ttk.Notebook(self.edit_frame)
        self.vmap_text = SText(self.vmap_frame)
        self.vxml_text = SText(self.vxml_frame)
        self.head_hook = ttk.Frame(self.head_frame)

        # Bind Frames to Tabs Widgets.
        self.home_hook.pack(side="top", fill="both", expand=True)
        self.joys_tabs.pack(side="top", fill="both", expand=True)
        self.edit_tabs.pack(side="top", fill="both", expand=True)
        self.vmap_text.pack(side="top", fill="both", expand=True)
        self.vxml_text.pack(side="top", fill="both", expand=True)
        self.head_hook.pack(side="top", fill="both", expand=True)

        # Populate the Tabs Widgets.
        self._set_home_widget()
        self._set_joys_widget()
        self._set_edit_widget()
        self._set_head_widget()

        # Bind "Window Closed" event.
        self.protocol("WM_DELETE_WINDOW", self.kill)

        # Bind "Tab Changed" event.
        self.tabs_pane.bind('<<NotebookTabChanged>>', self._command_tab_change)

        # Bind "Mouse Wheel" event.
        self.bind_all("<MouseWheel>", self._command_mousewheel)
        self.actual_scroll_canvas = None
        self.actual_scroll_frame = None

        # Attribute to hold and test ".after" programming.
        self.progr = None

    def start(self):
        """Create the Application window and run the Tk mainloop."""
        self.mainloop()

    def kill(self):
        """Stop the Application."""
        if self.progr: # Cancel any programmed command.
            self.after_cancel(self.progr)
        self.destroy()

    def _command_mousewheel(self, event):
        """Scroll the actual scrollable canvas if required."""
        if self.actual_scroll_canvas and self.actual_scroll_frame.winfo_reqheight() > self.actual_scroll_canvas.winfo_height():
            self.actual_scroll_canvas.yview_scroll(-1*(event.delta/120), "units")

    def _command_tab_change(self, event):
        """React on a main tab change."""
        if self.progr: # Cancel any programmed command.
            self.after_cancel(self.progr)
        target = self.tabs_pane.select()
        if   target == str(self.home_frame):
            self._home_tab()
        elif target == str(self.joys_frame):
            self._joys_tab()
        elif target == str(self.edit_frame):
            self._edit_tab()
        elif target == str(self.vmap_frame):
            self._vmap_tab()
        elif target == str(self.vxml_frame):
            self._vxml_tab()
        elif target == str(self.head_frame):
            self._head_tab()

    def _command_subtab_change(self, event):
        """React on a subtab change."""
        # Update the actual scrollable canvas.
        self.actual_scroll_canvas = self.nametowidget(event.widget.select()).canvas
        self.actual_scroll_frame = self.nametowidget(event.widget.select()).interior

    def _command_midtab_change(self, event):
        """React on a subtab change."""
        # Update the actual scrollable canvas.
        self.actual_scroll_canvas = self.nametowidget(self.nametowidget(event.widget.select()).select()).canvas
        self.actual_scroll_frame = self.nametowidget(self.nametowidget(event.widget.select()).select()).interior

    def _home_tab(self):
        """Activates HOME tab features."""
        self.actual_scroll_canvas = None
        self.actual_scroll_frame = None
        self.home_text.focus_set()

    def _joys_tab(self):
        """Activates JOYS tab features."""
        try:# Maybe there are no subtabs here.
            self.actual_scroll_canvas = self.nametowidget(self.joys_tabs.select()).canvas
            self.actual_scroll_frame = self.nametowidget(self.joys_tabs.select()).interior
        except Exception as ex:
            self.actual_scroll_canvas = None
            self.actual_scroll_frame = None
        self.progr = self.after(50, self._joys_update)

    def _edit_tab(self):
        """Activates EDIT tab features."""
        try:# Maybe there are no subtabs here.
            self.actual_scroll_canvas = self.nametowidget(self.nametowidget(self.edit_tabs.select()).select()).canvas
            self.actual_scroll_frame = self.nametowidget(self.nametowidget(self.edit_tabs.select()).select()).interior
        except Exception as ex:
            print('Exception {0}'.format(ex))
            self.actual_scroll_canvas = None
            self.actual_scroll_frame = None

    def _vmap_tab(self):
        """Activates MAP tab features."""
        self.actual_scroll_canvas = None
        self.actual_scroll_frame = None
        self._update_text(self.mapping.get_text(), self.vmap_text)
        self.vmap_text.focus_set()

    def _vxml_tab(self):
        """Activates XML tab features."""
        self.actual_scroll_canvas = None
        self.actual_scroll_frame = None
        self._update_text(self.mapping.get_xml(), self.vxml_text)
        self.vxml_text.focus_set()

    def _head_tab(self):
        """Activates XML tab features."""
        self.actual_scroll_canvas = self.modifier_keys_frame.canvas
        self.actual_scroll_frame = self.modifier_keys_frame.interior
        self.head_dialog.focus_set()

    # Home Functions.
    def _set_home_widget(self):
        """Set the "Home" widget."""
        # Create frames for content.
        left_frame = ttk.Frame(self.home_hook, relief='ridge', padding=5)
        left_frame.pack(side="left", fill="y")

        right_frame = ttk.Frame(self.home_hook, padding=2)
        right_frame.pack(side="left", fill="both", expand=True)

        # Create left content.
        self.home_cred = ttk.Label(left_frame, text=HOMETEXT, justify='left', width=38)
        self.home_cred.pack(side="top", fill="y")

        ttk.Button(left_frame, text=RELOAD, command=self._reset_joys_widget).pack(side="bottom", fill="x")

        # Create right content.

        # rigth_tabs = ttk.Notebook(right_frame, padding=5)
        # rigth_tabs.pack(side="top", fill="both", expand=True)
        # self.home_text = SText(rigth_tabs, padx=5)
        # rigth_tabs.add(self.home_text, text='Manual')

        self.home_text = SText(right_frame, padx=5)
        self.home_text.pack(side="top", fill="both", expand=True)

        self._update_text(MANUALTEXT, self.home_text)

    # Joys Functions.
    def _set_joys_widget(self):
        """
        Set the "Test" widget and return the vars to dump test data.
            Create a tab in the "Test" widget for each Joystick.
                Create a Label for each kind of actuator (Axis, Button, Hat) in Joystick tab.
                    Set the var of the Label.
        """
        self.joysticks = []
        self.joys_tabs.bind('<<NotebookTabChanged>>', self._command_subtab_change)
        font = tkFont.Font(family="lucida console", size=10)

        JoyList = [sdl2.SDL_JoystickOpen(joy) for joy in range(sdl2.SDL_NumJoysticks())]
        for joystick in  JoyList:
            joynum = sdl2.SDL_JoystickInstanceID(joystick)
            self.translator.remap_joy(joynum, joynum+1)
            # Create variables for data dump.
            hat_var      = tk.StringVar()
            axis_var     = tk.StringVar()
            info_var     = tk.StringVar()
            button_var   = tk.StringVar()

            hat_var.set('Collecting Data...')
            axis_var.set('Collecting Data...')
            info_var.set('Collecting Data...')
            button_var.set('Collecting Data...')
            name_var = sdl2.SDL_JoystickName(joystick)
            self.mapping.set_joy_name(str(joynum+1), name_var)

            # Set a tab for each joystick.
            joystick_frame = VSFrame(self.joys_tabs, padding=5)
            self.joys_tabs.add(joystick_frame, text='  {0}  '.format(name_var))

            # Create frames for data labels.
            upper_frame    = ttk.Frame(joystick_frame.interior)
            upper_frame.pack(side="top", fill="both")

            lower_frame    = ttk.Frame(joystick_frame.interior)
            lower_frame.pack(side="bottom", fill="both")

            info_frame    = ttk.LabelFrame(upper_frame, text=INFOTAB, padding=5)
            info_frame.pack(side="left", fill="both")

            buttons_frame = ttk.LabelFrame(upper_frame, text=BTTNTAB, padding=5)
            buttons_frame.pack(side="left", fill="both")

            axes_frame    = ttk.LabelFrame(upper_frame, text=AXESTAB, padding=5)
            axes_frame.pack(side="left", fill="both")

            hats_frame    = ttk.LabelFrame(upper_frame, text=HATSTAB, padding=5)
            hats_frame.pack(side="left", fill="both")

            # Create data labels.
            info_label    = ttk.Label(info_frame,    textvariable=info_var,     font=font)
            info_label.pack(side="top")

            buttons_label = ttk.Label(buttons_frame, textvariable=button_var,   font=font)
            buttons_label.pack(side="top")

            axes_label    = ttk.Label(axes_frame,    textvariable=axis_var,     font=font)
            axes_label.pack(side="top")

            hats_label    = ttk.Label(hats_frame,    textvariable=hat_var,      font=font)
            hats_label.pack(side="top")

            # Create an traceable var for js_input value.
            js_input = tk.StringVar()
            js_input.set(self.translator.translate_joy_num(joynum))

            # Create resources for axes translation switching.
            axesdicts  = cycle(self.translator.axesdicts)
            axesv      = tk.StringVar()
            axesv.set(self.translator.get_axes_map(name_var))

            def cycle_map(name, ax):
                ax.set(self.translator.remap_axes(name, axesdicts.next()))

            # Define a lambada functions for button/event behavior.
            bt_asign = lambda j=joynum, i=js_input: self.translator.remap_joy(j,i.get())
            cm_asign = lambda e=None, j=joynum, i=js_input: self.translator.remap_joy(j,i.get())
            ax_remap = lambda name=name_var, ax=axesv: cycle_map(name,ax)

            # Create the entry field.
            js_dialog = tk.Entry(info_frame, textvariable=js_input, justify="center", bd=3)
            js_dialog.select_range(0, tk.END)
            js_dialog.bind("<Return>", cm_asign)

            # Create the "Assign" button.
            asign_button = ttk.Button(info_frame, text=ASIGBTT, command=bt_asign)

            # Create the "Translation" Label.
            trans_label = ttk.Label(info_frame, text='\n\n{0}:'.format(TRANSAXE))

            # Create the "translation" button.
            axes_button  = ttk.Button(info_frame, textvariable=axesv, command=ax_remap)

            ## IGNORE FEATURE ##

            ignore_var = tk.StringVar()
            ignore_var.set(NIGNORE)

            def _swap_ignore(num, var):
                if num not in self.ignorelist:
                    self.ignorelist.add(num)
                    var.set(TIGNORE)
                else:
                    self.ignorelist.remove(num)
                    var.set(NIGNORE)

            bt_ingnore = lambda iv=ignore_var, num=joynum: _swap_ignore(num, iv)

            # Create the "Ignore" Label.
            ignore_label = ttk.Label(info_frame, text='\n\n{0}:'.format(IGNORE))

            # Create the "Ignore" button.
            ignore_button  = ttk.Button(info_frame, textvariable=ignore_var, command=bt_ingnore)

            ## END IGNORE FEATURE ##

            # Pack elements.
            js_dialog.pack(side="top", fill="x")
            asign_button.pack(side="top", fill="x")
            trans_label.pack(side="top")
            axes_button.pack(side="top", fill="x")
            ignore_label.pack(side="top")
            ignore_button.pack(side="top", fill="x")

            # ########################################################################################### #
            # ####                              Curves tweaking resources                            #### #
            # ########################################################################################### #
            ItsFloat = self.register(self.tryfloatval)

            # Create traceable vars for general options.
            deadX_var   = tk.DoubleVar()
            deadY_var   = tk.DoubleVar()
            deadZ_var   = tk.DoubleVar()
            deadRX_var  = tk.DoubleVar()
            deadRY_var  = tk.DoubleVar()
            deadRZ_var  = tk.DoubleVar()
            deadS1_var  = tk.DoubleVar()
            deadS2_var  = tk.DoubleVar()
            point_input = tk.DoubleVar()

            # Set general options default values.
            init_X, init_Y, init_Z, init_RX, init_RY, init_RZ, init_S1, init_S2 = self.mapping.get_deviceopt(name_var)
            deadX_var.set(init_X)
            deadY_var.set(init_Y)
            deadZ_var.set(init_Z)
            deadRX_var.set(init_RX)
            deadRY_var.set(init_RY)
            deadRZ_var.set(init_RZ)
            deadS1_var.set(init_S1)
            deadS2_var.set(init_S2)
            point_input.set(0.0)

            # Create a frame for general options.
            opts_frame = ttk.LabelFrame(upper_frame, text=OPGSTAB, padding=5)
            opts_frame.pack(side="left", fill="both")

            # Create frame for curve drawing
            curv_frame = ttk.LabelFrame(lower_frame, text=CURVTAB, padding=5)
            curv_frame.pack(side="left", fill="both")

            # Create widget for curve drawing.
            curv_grid = PGrid(curv_frame, width=350, height=350, bd=3, relief='ridge')
            curv_grid.pack(side="top")

            # Create frame for curve manipulation:
            slid_frame = ttk.LabelFrame(lower_frame, text=CURVSLI, padding=5)
            slid_frame.pack(side="left", fill="both")

            # Create subframe for curve manipulation buttons:
            buttons = ttk.Frame(slid_frame)
            buttons.pack(side="top", fill="both")

            # Create subframe for curve manipulation buttons:
            buttons_top = ttk.Frame(buttons)
            buttons_top.pack(side="top", fill="both")

            # Create frame for curve manipulation buttons:
            buttons_down = ttk.Frame(buttons)
            buttons_down.pack(side="top", fill="both")

            # Create widget container for curve manipulation sliders.
            slid_frame.sliders = ttk.Label(slid_frame, text="PLACEHOLDER", font=font)
            slid_frame.sliders.pack(side="bottom")

            # New code for dynamic curve vars creation.
            curve_names = ['flight_move_pitch', 'flight_move_yaw', 'flight_move_roll',
                           'flight_move_strafe_vertical', 'flight_move_strafe_lateral',
                           'flight_move_strafe_longitudinal', 'flight_aim_pitch', 'flight_aim_yaw',
                           'flight_view_pitch', 'flight_view_yaw', 'flight_zoom',
                           'turret_aim_pitch', 'turret_aim_yaw',
                           'flight_throttle_abs', 'flight_throttle_rel']
            curves = OrderedDict()
            act_curve = tk.StringVar()
            act_curve.set(curve_names[0])
            curve_text = tk.StringVar()
            curve_text.set('Showing {0}'.format(act_curve.get().title()))

            # Get initial curves values.
            init_curves = self.mapping.options.get(self.translator.translate_joy_num(joynum),{})
            curves_fr = HSFrame(upper_frame, padding=5)
            curves_fr.pack(side="top", expand=True, fill='both')
            for curve in curve_names:
                # Create traceable vars for field values.
                invert      = tk.IntVar()
                exponent    = tk.DoubleVar()
                sensitivity = tk.DoubleVar()
                datadict    = {0.0:0.0, 1.0:1.0}

                # Set initial values.
                if curve in ['flight_throttle_abs', "flight_move_strafe_longitudinal"]:  # Make throttle defaults to invert=1
                    init_curve = init_curves.get(curve, {'invert':1, 'sense':1.0, 'exponent':1.0, 'data':{}})
                else:                               # Anything else defaults to invert=0
                    init_curve = init_curves.get(curve, {'invert':0, 'sense':1.0, 'exponent':1.0, 'data':{}})
                invert.set(init_curve['invert'])
                sensitivity.set(init_curve['sense'])
                exponent.set(init_curve['exponent'])

                # Create a frame for axis variable input.
                new_frame = ttk.LabelFrame(curves_fr.interior, text=curve.title(), padding=5)
                new_frame.pack(side="left", expand=True, fill="both")

                # Create the "Sensitivity" Label.
                ttk.Label(new_frame, text=SENSITI).pack(side="top")

                # Create the entry field for "Sensitivity".
                sense_field = tk.Entry(new_frame, textvariable=sensitivity, justify="center", bd=3, validate='all', validatecommand=(ItsFloat, '%P', 0, 2))
                sense_field.select_range(0, tk.END)
                sense_field.pack(side="top", fill="x")

                # Create the "Exponent" Label.
                ttk.Label(new_frame, text=EXPONEN).pack(side="top")

                # Create the entry field for "Exponent".
                expon_field = tk.Entry(new_frame, textvariable=exponent, justify="center", bd=3, validate='all', validatecommand=(ItsFloat, '%P', 0, 2))
                expon_field.select_range(0, tk.END)
                expon_field.pack(side="top", fill="x")

                # Define a lambada function for "Invert".
                bt_invert = lambda x=invert: x.set((x.get()+1)%2)

                # Create the "Invert" button.
                ttk.Button(new_frame, text=INVERT, command=bt_invert).pack(side="top", fill="x")

                # Pack the new axis in the curves list.
                curves[curve] = {'invert':invert, 'sense':sensitivity, 'exponent':exponent, 'data':datadict}

            # Create lambda functions.
            bt_set_deadzns  = lambda d=name_var, x=deadX_var, y=deadY_var, z=deadZ_var, rx=deadRX_var, ry=deadRY_var, rz=deadRZ_var, s1=deadS1_var, s2=deadS2_var: self.mapping.set_deviceopt(
                                     d, _getvar(x, 0.015), _getvar(y, 0.015), _getvar(z, 0.015),
                                        _getvar(rx, 0.015), _getvar(ry, 0.015), _getvar(rz, 0.015),
                                        _getvar(s1, 0.015), _getvar(s2, 0.015))
            bt_del_deadzns  = lambda d=name_var : self.mapping.del_deviceopt(d)
            bt_next_curve   = lambda n=curve_names, c=curves, a=act_curve, t=curve_text, f=slid_frame: _next_curve(n, c, a, t, f)
            cm_add_point    = lambda e=None, x=point_input, c=curves, a=act_curve, f=slid_frame: _new_point(x, c, a, f)
            bt_add_point    = lambda x=point_input, c=curves, a=act_curve, f=slid_frame: _new_point(x, c, a, f)
            bt_del_point    = lambda x=point_input, c=curves, a=act_curve, f=slid_frame: _del_point(x, c, a, f)
            bt_set_options  = lambda j=joynum, c=curves : self.mapping.set_options(self.translator.translate_joy_num(j), _norm_dict(c))
            bt_del_options  = lambda j=joynum : self.mapping.del_options(self.translator.translate_joy_num(j))

            # No deadzones coded for XboxPad.
            if not name_var.startswith('XInput Controller'):
                # Create the "Deadzone X" Label.
                ttk.Label(opts_frame, text='{0} X:'.format(DEADZON)).pack(side="top")
                # Create the entry field for Deadzone X.
                deadX_field = tk.Entry(opts_frame, textvariable=deadX_var, justify="center", bd=3, validate='all', validatecommand=(ItsFloat, '%P', 0, 1))
                deadX_field.select_range(0, tk.END)
                deadX_field.pack(side="top", fill="x")

                # Create the "Deadzone Y" Label.
                ttk.Label(opts_frame, text='{0} Y:'.format(DEADZON)).pack(side="top")
                # Create the entry field for Deadzone Y.
                deadY_field = tk.Entry(opts_frame, textvariable=deadY_var, justify="center", bd=3, validate='all', validatecommand=(ItsFloat, '%P', 0, 1))
                deadY_field.select_range(0, tk.END)
                deadY_field.pack(side="top", fill="x")

                # Create the "Deadzone Z" Label.
                ttk.Label(opts_frame, text='{0} Z:'.format(DEADZON)).pack(side="top")
                # Create the entry field for Deadzone Y.
                deadY_field = tk.Entry(opts_frame, textvariable=deadZ_var, justify="center", bd=3, validate='all', validatecommand=(ItsFloat, '%P', 0, 1))
                deadY_field.select_range(0, tk.END)
                deadY_field.pack(side="top", fill="x")

                # Create the "Deadzone RotX" Label.
                ttk.Label(opts_frame, text='{0} RotX:'.format(DEADZON)).pack(side="top")
                # Create the entry field for Deadzone RotX.
                deadY_field = tk.Entry(opts_frame, textvariable=deadRX_var, justify="center", bd=3, validate='all', validatecommand=(ItsFloat, '%P', 0, 1))
                deadY_field.select_range(0, tk.END)
                deadY_field.pack(side="top", fill="x")

                # Create the "Deadzone RotY" Label.
                ttk.Label(opts_frame, text='{0} RotY:'.format(DEADZON)).pack(side="top")
                # Create the entry field for Deadzone RotY.
                deadY_field = tk.Entry(opts_frame, textvariable=deadRY_var, justify="center", bd=3, validate='all', validatecommand=(ItsFloat, '%P', 0, 1))
                deadY_field.select_range(0, tk.END)
                deadY_field.pack(side="top", fill="x")

                # Create the "Deadzone RotZ" Label.
                ttk.Label(opts_frame, text='{0} RotZ:'.format(DEADZON)).pack(side="top")
                # Create the entry field for Deadzone RotZ.
                deadY_field = tk.Entry(opts_frame, textvariable=deadRZ_var, justify="center", bd=3, validate='all', validatecommand=(ItsFloat, '%P', 0, 1))
                deadY_field.select_range(0, tk.END)
                deadY_field.pack(side="top", fill="x")

                # Create the "Deadzone slider1" Label.
                ttk.Label(opts_frame, text='{0} slider1:'.format(DEADZON)).pack(side="top")
                # Create the entry field for Deadzone slider1.
                deadY_field = tk.Entry(opts_frame, textvariable=deadS1_var, justify="center", bd=3, validate='all', validatecommand=(ItsFloat, '%P', 0, 1))
                deadY_field.select_range(0, tk.END)
                deadY_field.pack(side="top", fill="x")

                # Create the "Deadzone slider2" Label.
                ttk.Label(opts_frame, text='{0} slider2:'.format(DEADZON)).pack(side="top")
                # Create the entry field for Deadzone slider2.
                deadY_field = tk.Entry(opts_frame, textvariable=deadS2_var, justify="center", bd=3, validate='all', validatecommand=(ItsFloat, '%P', 0, 1))
                deadY_field.select_range(0, tk.END)
                deadY_field.pack(side="top", fill="x")

                # Create the "Set Deadzones" button.
                ttk.Button(opts_frame, text=SETDBTN, command=bt_set_deadzns).pack(side="top", fill="x")
                # Create the "Reset Deadzones" button.
                ttk.Button(opts_frame, text=USTDBTN, command=bt_del_deadzns).pack(side="top", fill="x")
            # Create the "Reset Options" button.
            ttk.Button(opts_frame, text=USTOBTN, command=bt_del_options).pack(side="bottom", fill="x")
            # Create the "Set Options" button.
            ttk.Button(opts_frame, text=SETOBTN, command=bt_set_options).pack(side="bottom", fill="x")

            # Create lambda function for help pop up.
            bt_help = lambda : MBox.showinfo(HELPCBT, CURVEHELP)
            # Create the "Help" button.
            ttk.Button(buttons_top, text=HELPCBT, command=bt_help).pack(side="left", fill="x")
            # Create the Next Curve" button.
            ttk.Button(buttons_top, text=NEXTCV, command=bt_next_curve).pack(side="left", fill="x")
            # Create the "Current curve" Label.
            ttk.Label(buttons_top, justify="center", textvariable=curve_text).pack(side="left")
            # Create the "Add Point" button.
            ttk.Button(buttons_down, text=ADDPNT, command=bt_add_point).pack(side="left", fill="x")
            # Create the entry field for "New point".
            point_field = tk.Entry(buttons_down, textvariable=point_input, justify="center", bd=3, validate='all', validatecommand=(ItsFloat, '%P', 0, 1))
            point_field.select_range(0, tk.END)
            point_field.bind("<Return>", cm_add_point)
            point_field.pack(side="left", fill="x")
            # Create the "Del Point" button.
            ttk.Button(buttons_down, text=DELPNT, command=bt_del_point).pack(side="left", fill="x")

            def _getvar(var, default):
                """Return the value of a tk.variable or a default value if there is an error."""
                try:
                    val = var.get()
                except:
                    return default
                else:
                    return val

            def _norm_dict(curves):
                """Returns a dict of direct values extracted from "curves" Tk.variables."""
                out_dict = OrderedDict()
                for curve, options in curves.items():
                    invert      = options['invert'].get()
                    exponent    = options['exponent'].get()
                    sensitivity = options['sense'].get()
                    datadict    = {}
                    for Xpoint, Yvar in sorted(options['data'].items()):
                        if 0.0 < Xpoint < 1.0:
                            datadict[Xpoint] = Yvar.get()
                        else:
                            datadict[Xpoint] = Yvar
                    out_dict[curve] = {'invert':invert, 'sense':sensitivity, 'exponent':exponent, 'data':datadict}
                return out_dict

            def _next_curve(names, curves, actual, text, frame):
                """Make selected the next curve."""
                actual.set(names[(names.index(actual.get())+1)%len(names)])
                text.set('Showing {0}'.format(actual.get().title()))
                _redraw_sliders(curves, actual, frame)

            def _redraw_sliders(curves, actual, frame):
                """Redraw curve control points sliders."""
                frame.sliders.destroy()
                frame.sliders = ttk.Frame(frame)
                frame.sliders.pack(side="bottom", fill="both")
                for Xpoint, Yvar in sorted(curves[actual.get()]['data'].items()):
                    if 0.0 < Xpoint < 1.0:
                        tk.Scale(frame.sliders, from_=1, to=0, resolution=0.001, variable=Yvar,
                                 orient="vertical", length=350, label=Xpoint).pack(side="left")

            def _new_point(Xvar, curves, actual, frame):
                """Add a control point to actual curve."""
                Xpoint = _getvar(Xvar, 0.0)
                if 0.0 < Xpoint < 1.0:
                    Yvar = tk.DoubleVar()
                    Yvar.set(Xpoint)
                    curves[actual.get()]['data'][Xpoint] = Yvar
                    _redraw_sliders(curves, actual, frame)

            def _del_point(Xvar, curves, actual, frame):
                """Delete a control point to actual curve."""
                Xpoint = _getvar(Xvar, 0.0)
                if 0.0 < Xpoint < 1.0:
                    curves[actual.get()]['data'].pop(Xpoint, False)
                    _redraw_sliders(curves, actual, frame)

            def _add_point(Xpoint, Ypoint, curve):
                """Add a given control point to the given curve."""
                if 0.0 < Xpoint < 1.0:
                    Yvar=tk.DoubleVar()
                    Yvar.set(Ypoint)
                    curve[Xpoint]=(Yvar)

            # Create initial Sliders.
            for curve, options in curves.items():
                init_curve = init_curves.get(curve, {'data':{0.25:0.25, 0.5:0.5, 0.75:0.75}})
                for Xpoint, Ypoint in init_curve['data'].items():
                    _add_point(Xpoint, Ypoint, options['data'])
            _redraw_sliders(curves, act_curve, slid_frame)

            # Update joysticks data vars vector.
            self.joysticks.insert(0,(joystick, axis_var, button_var, hat_var, info_var, name_var, curves, act_curve, curv_grid))

    def _reset_joys_widget(self):
        """Resets the "Test" widget to an empty state and reload Joysticks."""
        if self.progr: # Cancel any programmed command.
            self.after_cancel(self.progr)
        self.joys_tabs.destroy()
        self.joys_tabs = ttk.Notebook(self.joys_frame)
        self.joys_tabs.pack(side="top", fill="both", expand=True)
        self._set_joys_widget()
        self._joys_update(after=self.tabs_pane.select() == str(self.joys_frame))

    def _joys_update(self, after=True):
        """Update the test data."""
        sdl2.SDL_PumpEvents()
        for joy, joy_axes, joy_butt, joy_hats, joy_info, joy_name, joy_curves, joy_act_curve, drawing_grid in self.joysticks:
            text = ''
            text += '{0}: {1}\n\n'.format(GAMEID, self.translator.translate_joy(sdl2.SDL_JoystickInstanceID(joy)))
            text += 'GUID: \n'
            GUID = sdl2.SDL_JoystickGetGUID(joy)
            GUIDdata = iter(['{0:0>2X}'.format(i) for i in GUID.data])
            for data in GUIDdata:
                text += '        {1}{0}\n'.format(data,GUIDdata.next())
            joy_info.set(text)
            axes = sdl2.SDL_JoystickNumAxes(joy)
            text = ''
            for axis in range(axes):
                value = sdl2.SDL_JoystickGetAxis(joy,axis)
                text += "{0} {1:12} {2:>4}\n".format(AXIS, self.translator.translate_axis(axis, joy_name),
                                                      self.translator.normalize_axis(axis, joy_name, value))
            joy_axes.set(text)
            buttons = sdl2.SDL_JoystickNumButtons(joy)
            text = ''
            for button in range(buttons):
                value = sdl2.SDL_JoystickGetButton(joy,button)
                text += "{0} {1:>2}:  {2}\n".format(BUTTON, self.translator.translate_button_num(button), value)
            joy_butt.set(text)
            hats = sdl2.SDL_JoystickNumHats(joy)
            text = ''
            for hat in range(hats):
                value = sdl2.SDL_JoystickGetHat(joy,hat)
                text += "{0} {1}: {3:>18}\n".format(HAT, self.translator.translate_hat_num(hat), VALUE, self.translator.translate_hat_pos(value))
            joy_hats.set(text)

            # Update graphics.
            invert = joy_curves[joy_act_curve.get()]['invert']
            sensitivity = joy_curves[joy_act_curve.get()]['sense']
            exponent = joy_curves[joy_act_curve.get()]['exponent']
            datadict = joy_curves[joy_act_curve.get()]['data']

            def _listed(datadict):
                """Return the list of points from datadict."""
                datalist=[]
                for Xpoint, Yvar in sorted(datadict.items()):
                    datalist.append(Xpoint)
                    if 0.0 < Xpoint < 1.0:
                        datalist.append(Yvar.get())
                    else:
                        datalist.append(Yvar)
                return datalist

            try:
                datalist = _listed(datadict)
                inv = invert.get()
                sen = sensitivity.get()
                exp = exponent.get()
                drawing_grid.redrawCurve(datalist, inv, sen, exp)
            except:
                pass

        if after:
            self.progr = self.after(50, self._joys_update)

    # Edit Functions.
    def _set_edit_widget(self):
        """
        Set the "Edit" widget.
            Create a tab in the "Edit" widget for each actionmap.
                Create a frame for each action in actionmap tab.
                    Set the contents of the frame.
        """
        spaceship_1_tab = ['spaceship_general', 'spaceship_view', 'spaceship_movement', 'spaceship_targeting', 'spaceship_weapons', 'spaceship_missiles']
        spaceship_2_tab = ['spaceship_turret', 'spaceship_defensive', 'spaceship_auto_weapons', 'spaceship_radar', 'spaceship_hud', 'spaceship_power']
        edit_gntab = ttk.Notebook(self.edit_tabs)
        edit_s1tab = ttk.Notebook(self.edit_tabs)
        edit_s2tab = ttk.Notebook(self.edit_tabs)
        self.edit_tabs.add(edit_gntab, text='General')
        self.edit_tabs.add(edit_s1tab, text='Spaceship_1')
        self.edit_tabs.add(edit_s2tab, text='Spaceship_2')
        self.edit_tabs.bind('<<NotebookTabChanged>>', self._command_midtab_change)
        edit_gntab.bind('<<NotebookTabChanged>>', self._command_subtab_change)
        edit_s1tab.bind('<<NotebookTabChanged>>', self._command_subtab_change)
        edit_s2tab.bind('<<NotebookTabChanged>>', self._command_subtab_change)
        actionmaps = self.mapping.actionmaps.items()
        for actionmap_name, actionmap in self.mapping.actionmaps.items():
            # Set and select tab groups and set a tab for each actionmap.
            gn_actionmap_frame = VSFrame(edit_gntab, padding=5)
            s1_actionmap_frame = VSFrame(edit_s1tab, padding=5)
            s2_actionmap_frame = VSFrame(edit_s2tab, padding=5)
            if actionmap_name in spaceship_1_tab:
                edit_s1tab.add(s1_actionmap_frame, text=' {0} '.format(actionmap_name))
                actionmap_frame = s1_actionmap_frame
            elif actionmap_name in spaceship_2_tab:
                edit_s2tab.add(s2_actionmap_frame, text=' {0} '.format(actionmap_name))
                actionmap_frame = s2_actionmap_frame
            else:
                edit_gntab.add(gn_actionmap_frame, text=' {0} '.format(actionmap_name))
                actionmap_frame = gn_actionmap_frame
            for action_name, action_dict in actionmap.items():
                # Set a frame for each action.
                action_frame = ttk.LabelFrame(actionmap_frame.interior, padding=5, labelanchor='n')
                action_frame.pack(side="top", fill="both", expand=True)
                header_frame = ttk.Frame(action_frame)
                header_frame.pack(side="top", fill="both", expand=True)

                # Create the "Description" frame.
                action_desc = ttk.LabelFrame(header_frame, text=action_name, labelanchor='n')
                action_desc.pack(side="left", fill="both", expand=True)

                # Set the "Description" label.
                action_label = ttk.Label(action_desc, text='{0:>15}'.format(ACTIONDESCS.get(action_name, 'Not Set')))
                action_label.pack(side="left", fill="both")

                # Create an traceable var for lines number.
                lines = tk.IntVar()
                lines.set(0)

                # Define lambada functions for button behavior.
                nb_comm = lambda af=action_frame, am=actionmap_name, an=action_name, l=lines: self._new_bind_line(af, am, an, l)

                # Create the "New binding" button.
                newbind_button = ttk.Button(header_frame, text=NEWBIND, command=nb_comm)
                newbind_button.pack(side="left")

                # Create initial binding lines.
                for binding in action_dict['input']:
                    self._base_bind_line(action_frame, action_dict['device'], action_dict['input'][binding], actionmap_name, action_name, lines)

    def _reset_edit_widget(self):
        """Reset the "Edit" widget to an empty state and reload the actual mapping."""
        self.edit_tabs.destroy()
        self.edit_tabs = ttk.Notebook(self.edit_frame)
        self.edit_tabs.pack(side="top", fill="both", expand=True)
        self._set_edit_widget()

    def _base_bind_line(self, action_frame, device, input, actionmap_name, action_name, lines):
        """Create a line for action binding."""
        # Create a frame for each bind.
        bind_frame = ttk.Frame(action_frame)
        bind_frame.pack(side="top", fill="both", expand=True)

        # Create an traceable var for binding value.
        action_bind = tk.StringVar()
        action_bind.set('{0:>15}'.format(input))

        # Create an traceable var for line number.
        line_number = tk.IntVar()
        line_number.set(lines.get())

        # Set a "Capture" button text.
        if   ACTIONMODES.get(action_name) == 'axis':
            bt_text = '{0}'.format(CAPAXIS)
        elif ACTIONMODES.get(action_name) == 'button':
            bt_text = '{0}'.format(CAPBTT)
        else:
            bt_text = '{0}'.format(CAPSMTH)

        # Define lambada functions for button behavior.
        bt_comm = lambda am=actionmap_name, an=action_name, b=action_bind, mo=ACTIONMODES.get(action_name,'something'), l=line_number: self._map_event(am,an,mo,b,l)
        bt_dire = lambda am=actionmap_name, an=action_name, b=action_bind, l=line_number: self._map_direct(am,an,b,l)
        bt_empt = lambda am=actionmap_name, an=action_name, b=action_bind, l=line_number: self._map_empty(am,an,b,l)
        bt_modf = lambda am=actionmap_name, an=action_name, b=action_bind, l=line_number: self._add_modifier(am,an,b,l)
        db_comm = lambda bf=bind_frame, am=actionmap_name, an=action_name, l=line_number : self._del_bind_line(bf, am, an, l)

        # Create the "Actual Binding" frame.
        action_input = ttk.LabelFrame(bind_frame, text=ACTBLAB)
        action_input.pack(side="left", fill="both", expand=True)

        # Set the "Actual Binding" label.
        action_label = ttk.Label(action_input, textvariable=action_bind)
        action_label.pack(side="left", fill="both", expand=True)

        # Create the "Capture" button.
        action_button = ttk.Button(bind_frame, text=bt_text, command=bt_comm, width=-25)
        action_button.pack(side="left", fill="x")

        # Create the "Direct" button.
        dinput_button = ttk.Button(bind_frame, text=DINPBTT, command=bt_dire, width=-25)
        dinput_button.pack(side="left", fill="x")

        # Create the "Clear" button.
        clearI_button = ttk.Button(bind_frame, text=CLSBTT, command=bt_empt, width=-25)
        clearI_button.pack(side="left", fill="x")

        # Create the "Modifier" button.
        clearI_button = ttk.Button(bind_frame, text=MDFBTT, command=bt_modf, width=-25)
        clearI_button.pack(side="left", fill="x")

        # Create the "Delete binding" button.
        delbind_button = ttk.Button(bind_frame, text=DELBIND, command=db_comm)
        delbind_button.pack(side="left")
        if lines.get() == 0:
            delbind_button.state(['disabled'])
            delbind_button.configure(text=LOCKED)
            if device == "keyboard":
                action_button.state(['disabled'])
                action_button.configure(text=KBBOUND)

        # Increase lines number.
        lines.set(lines.get()+1)

    def _new_bind_line(self, action_frame, actionmap_name, action_name, lines):
        """Create a new line for action binding."""
        # Create a frame for each bind.
        bind_frame = ttk.Frame(action_frame)
        bind_frame.pack(side="top", fill="both", expand=True)

        # Create an traceable var for binding value.
        action_bind = tk.StringVar()
        action_bind.set('{0:>15}'.format(NULLINP))

        # Create an traceable var for line number.
        line_number = tk.IntVar()
        line_number.set(lines.get())

        # Set a "Capture" button text.
        if   ACTIONMODES.get(action_name) == 'axis':
            bt_text = '{0}'.format(CAPAXIS)
        elif ACTIONMODES.get(action_name) == 'button':
            bt_text = '{0}'.format(CAPBTT)
        else:
            bt_text = '{0}'.format(CAPSMTH)

        # Define lambada functions for button behavior.
        bt_comm = lambda am=actionmap_name, an=action_name, b=action_bind, mo=ACTIONMODES.get(action_name,'something'), l=line_number: self._map_event(am,an,mo,b,l)
        bt_dire = lambda am=actionmap_name, an=action_name, b=action_bind, l=line_number: self._map_direct(am,an,b,l)
        bt_empt = lambda am=actionmap_name, an=action_name, b=action_bind, l=line_number: self._map_empty(am,an,b,l)
        bt_modf = lambda am=actionmap_name, an=action_name, b=action_bind, l=line_number: self._add_modifier(am,an,b,l)
        db_comm = lambda bf=bind_frame, am=actionmap_name, an=action_name, l=line_number : self._del_bind_line(bf, am, an, l)

        # Create the "Actual Binding" frame.
        action_input = ttk.LabelFrame(bind_frame, text=ACTBLAB)
        action_input.pack(side="left", fill="both", expand=True)

        # Set the "Actual Binding" label.
        action_label = ttk.Label(action_input, textvariable=action_bind)
        action_label.pack(side="left", fill="both", expand=True)

        # Create the "Capture" button.
        action_button = ttk.Button(bind_frame, text=bt_text, command=bt_comm, width=-25)
        action_button.pack(side="left", fill="x")

        # Create the "Direct" button.
        dinput_button = ttk.Button(bind_frame, text=DINPBTT, command=bt_dire, width=-25)
        dinput_button.pack(side="left", fill="x")

        # Create the "Clear" button.
        clearI_button = ttk.Button(bind_frame, text=CLSBTT, command=bt_empt, width=-25)
        clearI_button.pack(side="left", fill="x")

        # Create the "Modifier" button.
        clearI_button = ttk.Button(bind_frame, text=MDFBTT, command=bt_modf, width=-25)
        clearI_button.pack(side="left", fill="x")

        # Create the "Delete binding" button.
        delbind_button = ttk.Button(bind_frame, text=DELBIND, command=db_comm)
        delbind_button.pack(side="left")

        # Increase lines number.
        lines.set(lines.get()+1)

        # Bind default value.
        self.mapping.set_action_input(actionmap_name, action_name, NULLINP, line_number.get())

    def _del_bind_line(self, bind_frame, actionmap_name, action_name, line):
        """Delete a line for action binding."""
        self.mapping.del_action_input(actionmap_name, action_name, line.get())
        bind_frame.destroy()

    def _map_event(self, actionmap, action, mode, label, line):
        """Map an input to an action."""

        def _capture(mode, msgtxt):
            """Capture a joystick actuation, without blocking wait, and map it."""
            # Try to capture the "Event" from joystick.if event.jhat.value in [sdl2.SDL_HAT_UP, sdl2.SDL_HAT_RIGHT, sdl2.SDL_HAT_DOWN, sdl2.SDL_HAT_LEFT]:
            if   mode == 'axis':
                event = self._capture_axis()
            elif mode == 'button':
                event = self._capture_button()
            elif mode == 'something':
                event = self._capture_something()

            if not event:
                # Retry.
                self._flush_events(mode)
                self.progr = self.after(10, _capture, mode, msgtxt)
            else:
                # Translate the "Event" to the Star Citizen naming.
                joy_name = self.joysticks[event.which][5]
                binding  = self.translator.translate_event(event, joy_name)
                # Check for collisions.
                collisions = self.mapping.get_input(binding)
                if collisions and '{0}.{1}'.format(actionmap, action) not in collisions:
                    collidetext = '\n'.join(collisions)
                    # Show captured binding.
                    msgtxt.set('{0}:\n{1}\n{2}\n\n{3}:\n\n{4}\n\n{5}'.format(CAPTMSG, joy_name, binding, COLLMSG, collidetext, PRESMSG))
                    # Create lambda function for "Done" button.
                    done_comm = lambda B=binding, T=0:_bind(B,T)
                    # Create the "Done" button.
                    done_button = ttk.Button(capture_window, text=DONE, command=done_comm)
                    done_button.pack(side="left", fill="x", expand=True)
                else:
                    # Show captured binding.
                    msgtxt.set('{0}:\n{1}\n{2}'.format(CAPTMSG, joy_name, binding))
                    cancel_button.destroy()
                    _bind(binding, 750)

        def _bind(binding, timer):
            # Update the actual "Mapping"
            self.mapping.set_action_input(actionmap, action, binding, line.get())

            # Update the "Actual Binding" label.
            label.set('{0:>15}'.format(binding))

            # Close the window.
            self.progr = self.after(timer, _exit, msgtxt)

        def _exit(*args):
            """Exit without changes."""
            if self.progr: # Cancel any programmed command.
                self.after_cancel(self.progr)
            capture_window.grab_release()
            capture_window.destroy()

        # Create the input window and lock interacting with the main window.
        capture_window = tk.Toplevel(padx=5, pady=5)
        capture_window.title(' *{0}*'.format(action))
        capture_window.resizable(width=False, height=False)
        capture_window.protocol("WM_DELETE_WINDOW", _exit)
        capture_window.grab_set()

        # Create the message.
        msgtxt = tk.StringVar()
        message = ttk.Label(capture_window, padding=5, justify="center", textvariable=msgtxt)
        message.pack(side="top")

        # Set message to show.
        if   mode == 'axis':
            msgtxt.set('{0} "{1}.{2}"\n'.format(AXISMSG, actionmap, action))
        elif mode == 'button':
            msgtxt.set('{0} "{1}.{2}"\n'.format(BTTNMSG, actionmap, action))
        elif mode == 'something':
            msgtxt.set('{0} "{1}.{2}"\n'.format(SMTHMSG, actionmap, action))

        # Create the "Cancel" button.
        cancel_button = ttk.Button(capture_window, text=CANCEL, command=_exit)
        cancel_button.pack(side="bottom", fill="x", expand=True)

        # Program capture.
        _log_data('Start "{0}" Capture:'.format(mode))
        self._flush_events()
        self.progr = self.after(10, _capture, mode, msgtxt)

    def _map_direct(self, actionmap, action, label, line):
        """Map a direct input to an action."""

        def _done(*args):
            """Map the input and exit."""
            binding = input.get()
            # Check for collisions.
            collisions = self.mapping.get_input(binding)
            if collisions and '{0}.{1}'.format(actionmap, action) not in collisions:
                collidetext = '\n'.join(collisions)
                # Show captured binding.
                msgtxt.set('{0}:\n{1}\n\n{2}:\n\n{3}\n\n{4}'.format(CAPTMSG, binding, COLLMSG, collidetext, PRESMSG))
                # Clear input widgets.
                dialog.pack_forget()
                input_button.pack_forget()
                tips_frame.pack_forget()
                # Create lambda function for "Done" button.
                done_comm = lambda B=binding, T=0: _bind(B,T)
                rtrn_comm = lambda Ev, B=binding, T=0: _bind(B,T)
                # Create the "Done" button.
                done_button = ttk.Button(input_frame, text=DONE, command=done_comm)
                done_button.pack(side="left", fill="x", expand=True)
                input_window.bind("<Return>", rtrn_comm)
            else:
                _bind(binding, 0)

        def _bind(binding, timer):
            # Update the actual "Mapping"
            self.mapping.set_action_input(actionmap, action, binding, line.get())

            # Update the "Actual Binding" label.
            label.set('{0:>15}'.format(binding))

            # Close the window.
            self.progr = self.after(timer, _exit, msgtxt)

        def _exit(*args):
            """Exit without changes."""
            if self.progr: # Cancel any programmed command.
                self.after_cancel(self.progr)
            input_window.grab_release()
            input_window.destroy()

        # Create the input window and lock interacting with the main window.
        input_window = tk.Toplevel(padx=5, pady=5)
        input_window.title(' *{0}*'.format(action))
        input_window.resizable(width=False, height=False)
        input_window.protocol("WM_DELETE_WINDOW", _exit)
        input_window.bind("<Return>", _done)
        input_window.grab_set()

        input_frame = ttk.Frame(input_window)
        input_frame.pack(side="top", fill="both")
        tips_frame = ttk.Frame(input_window)
        tips_frame.pack(side="bottom", fill="both")

        # Create the message for the tips.
        ttk.Label(tips_frame, padding=5, justify="center", text=DIHELPT).pack(side="top", expand=True)
        ttk.Label(tips_frame, padding=5, text=DIHELPL).pack(side="left", fill="y")
        ttk.Label(tips_frame, padding=5, text=DIHELPR).pack(side="left")

        # Create the message for the window.
        msgtxt = tk.StringVar()
        msgtxt.set(DIRIMSG)
        message = ttk.Label(input_frame, padding=5, justify="center", textvariable=msgtxt)
        message.pack(side="top", fill="both")

        # Create an traceable var for input value.
        input = tk.StringVar()
        input.set(label.get().strip())

        # Create the entry field, make it focused and selected.
        dialog = tk.Entry(input_frame, textvariable=input, justify="center", bd=3)
        dialog.pack(side="top", fill="x", expand=True)
        dialog.select_range(0, tk.END)
        dialog.focus_set()

        # Create the "Done" button.
        input_button = ttk.Button(input_frame, text=DONE, command=_done)
        input_button.pack(side="left", fill="x", expand=True)

        # Create the "Cancel" button.
        cancel_button = ttk.Button(input_frame, text=CANCEL, command=_exit)
        cancel_button.pack(side="right", fill="x", expand=True)

    def _map_empty(self, actionmap, action, label, line):
        """Map an null input to an action."""
        # Update the actual "Mapping"
        self.mapping.set_action_input(actionmap, action, NULLINP, line.get())
        # Update the "Actual Binding" label.
        label.set('{0:>15}'.format(NULLINP))

    def _add_modifier(self, actionmap, action, label, line):
        """Add a modifier to an action."""

        def _capture(*args):
            done_button.state(['disabled'])
            capture_button.state(['disabled'])
            input.set(CAPTING)
            dialog.config(state = tk.DISABLED)
            event = self._capture_button()
            if not event:
                # Retry.
                self._flush_events('button')
                self.progr = self.after(10, _capture, 'button', msgtxt)
            else:
                # Translate the "Event" to the Star Citizen naming.
                joy_name = self.joysticks[event.which][5]
                binding  = self.translator.translate_event(event, joy_name)
                input.set(binding)
                dialog.config(state = tk.NORMAL)
                done_button.state(['!disabled'])
                capture_button.state(['!disabled'])

        def _done(*args):
            """Map the input and exit."""
            # TODO: Check if the modifier has already been defined, if not, reject o warn???
            binding = '{0}+{1}'.format(input.get(), label.get().strip())
            # Check for collisions.
            collisions = self.mapping.get_input(binding)
            if collisions and '{0}.{1}'.format(actionmap, action) not in collisions:
                collidetext = '\n'.join(collisions)
                # Show captured binding.
                msgtxt.set('{0}:\n{1}\n\n{2}:\n\n{3}\n\n{4}'.format(CAPTMSG, binding, COLLMSG, collidetext, PRESMSG))
                # Clear input widgets.
                capture_button.pack_forget()
                dialog.pack_forget()
                done_button.pack_forget()
                tips_frame.pack_forget()
                # Create lambda function for "Done" button.
                done_comm = lambda B=binding, T=0: _bind(B,T)
                rtrn_comm = lambda Ev, B=binding, T=0: _bind(B,T)
                # Create the "Done" button.
                ok_button = ttk.Button(input_frame, text=DONE, command=done_comm)
                ok_button.pack(side="left", fill="x", expand=True)
                input_window.bind("<Return>", rtrn_comm)
            else:
                _bind(binding, 0)

        def _bind(binding, timer):
            # Update the actual "Mapping"
            self.mapping.set_action_input(actionmap, action, binding, line.get())

            # Update the "Actual Binding" label.
            label.set('{0:>15}'.format(binding))

            # Close the window.
            self.progr = self.after(timer, _exit, msgtxt)

        def _exit(*args):
            """Exit without changes."""
            if self.progr: # Cancel any programmed command.
                self.after_cancel(self.progr)
            input_window.grab_release()
            input_window.destroy()

        def _test_entry(postval, button):
            """Callback Function to test if the given text is empty and disable the button if so, otherwise enable it."""
            if postval not in self.mapping.get_modifiers(): #postval.isspace() or not postval or
                button.state(['disabled'])
            else:
                button.state(['!disabled'])
            return True

        # Create the input window and lock interacting with the main window.
        input_window = tk.Toplevel(padx=5, pady=5)
        input_window.title(' *{0}*'.format(action))
        input_window.resizable(width=False, height=False)
        input_window.protocol("WM_DELETE_WINDOW", _exit)
        input_window.bind("<Return>", _done)
        input_window.grab_set()

        input_frame = ttk.Frame(input_window)
        input_frame.pack(side="top", fill="both")
        tips_frame = ttk.Frame(input_window)
        tips_frame.pack(side="bottom", fill="both")

        # Create the message for the tips.
        ttk.Label(tips_frame, padding=5, justify="center", text=ADDMOD).pack(side="top", expand=True)

        # Create the message for the window.
        msgtxt = tk.StringVar()
        msgtxt.set(DIRIMSG)
        message = ttk.Label(input_frame, padding=5, justify="center", textvariable=msgtxt)
        message.pack(side="top", fill="both")

        # Create a lambda for capture.
        cap_btn = lambda : [self._flush_events(), _capture()]

        # Create the "Capture" button.
        capture_button = ttk.Button(input_frame, text=CAPTURE, command=cap_btn)
        capture_button.pack(side="top", fill="x", expand=True)

        # Create an traceable var for input value.
        input = tk.StringVar()
        input.set('')

        # Create the "Done" button.
        done_button = ttk.Button(input_frame, text=DONE, command=_done)

        # Create the "Cancel" button.
        cancel_button = ttk.Button(input_frame, text=CANCEL, command=_exit)

        # Create the entry field, make it focused and selected.
        lambda_test = lambda t, b=done_button: _test_entry(t, b)
        ItsNoSpace = self.register(lambda_test)
        dialog = tk.Entry(input_frame, textvariable=input, justify="center", bd=3, width=-35, validate='all', validatecommand=(ItsNoSpace, '%P'))
        dialog.pack(side="top", fill="x", expand=True)
        dialog.select_range(0, tk.END)
        dialog.focus_set()

        # Pack buttons.
        done_button.pack(side="left", fill="x", expand=True)
        cancel_button.pack(side="right", fill="x", expand=True)

        # Prepare capture.
        self._flush_events()

    # Header & Modifiers Functions.
    def _set_head_widget(self):
        """Set the "Header" tab."""
        self.header_var = tk.StringVar()
        self.header_var.set('Enter your Layout name')

        def clear_head():
            self.mapping.del_head()
            self.header_var.set('')

        # Create a frame for header section.
        header_frame = ttk.LabelFrame(self.head_hook, text=HEADER, padding=5, labelanchor='n')

        # Create frames for header items.
        buttons_frame = ttk.Frame(header_frame)

        # Create a "warning" for header usage.
        warn_label = tk.Label(header_frame, text=HEADWARN, justify="center")

        # Define a lambada functions for button/event behavior.
        bt_set = lambda h=self.header_var: self.mapping.set_head(h.get())
        cm_set = lambda e=None, h=self.header_var: self.mapping.set_head(h.get())

        # Create entry field for name.
        self.head_dialog = tk.Entry(header_frame, textvariable=self.header_var, justify="center", bd=3, width=75)
        self.head_dialog.bind("<Return>", cm_set)

        # Create the "Set" button.
        set_button = ttk.Button(buttons_frame, text=SETHBTN, command=bt_set)

        # Create the "Clear" button.
        clr_button = ttk.Button(buttons_frame, text=USTHBTN, command=clear_head)

        # Pack all items.
        header_frame.pack(side="top", fill='x')
        warn_label.pack(side="top", fill='x')
        self.head_dialog.pack(side='top', fill='x')
        buttons_frame.pack(side="top", fill='x')
        set_button.pack(side='left', fill='x', expand=True)
        clr_button.pack(side='left', fill='x', expand=True)

        # *********************************************************************** #
        # *                          Modifiers Section                          * #
        # *********************************************************************** #

        self.modifiers = []

        # Create a frame for modifiers section.
        modifiers_frame = ttk.LabelFrame(self.head_hook, text=MODIFIS, padding=5, labelanchor='n')

        # Create frames for header items.
        buttons_frame = ttk.Frame(modifiers_frame)

        # Create a labeled frame for modifiers listing.
        self.modifier_keys_frame = VSFrame(modifiers_frame, padding=5)

        # Define lambada function for new modifier key definition.
        new_modifier = lambda mf=self.modifier_keys_frame.interior: self._new_modifier_key(mf)

        # Create the "New Modifier" button.
        new_modifier_button = ttk.Button(buttons_frame, text=NEWMOD, command=new_modifier)

        # Pack all items.
        modifiers_frame.pack(side="top", fill='both', expand=True)
        buttons_frame.pack(side="top", fill='x')
        new_modifier_button.pack(side="top", fill="x")
        self.modifier_keys_frame.pack(side="top", fill="both", expand=True)

        # Declare initial modifiers.
        for modifier in ['xi1_shoulderl', 'xi1_shoulderr', 'xi1_triggerl_btn', 'xi1_triggerr_btn', 'xi1_back']:
            self._base_modifier_key(self.modifier_keys_frame.interior, modifier, fixed=True)
        for modifier in self.mapping.get_modifiers():
            self._base_modifier_key(self.modifier_keys_frame.interior, modifier, fixed=False)

    def _reset_head_widget(self):
        """Reset the "Head" widget to an empty state and reload the actual mapping."""
        self.head_hook.destroy()
        self.head_hook = ttk.Frame(self.head_frame)
        self.head_hook.pack(side="top", fill="both", expand=True)
        self._set_head_widget()

    def _base_modifier_key(self, modifiers_frame, key, fixed):
        """Add a new modifier key definition."""
        # Create the "Modifier" frame.
        modifier_frame = ttk.LabelFrame(modifiers_frame, padding=5)

        # Set the "Binding" label.
        key_label = ttk.Label(modifier_frame, text=key)

        # Pack all items.
        modifier_frame.pack(side="bottom", fill="both", expand=True)
        key_label.pack(side="left", fill="both", expand=True)

        if not fixed:
            # Define lambada function for del modifier.
            del_modifier = lambda target=modifier_frame, var=key: self._del_modifier_key(target, var)
            # Create the "Delete Modifier" button.
            del_modifier_button = ttk.Button(modifier_frame, text=DELMOD, command=del_modifier)
            del_modifier_button.pack(side="left", fill="x")

    def _new_modifier_key(self, modifiers_frame):
        """Add a new modifier key definition."""

        def _capture(*args):
            done_button.state(['disabled'])
            capture_button.state(['disabled'])
            input.set(CAPTING)
            dialog.config(state = tk.DISABLED)
            event = self._capture_button()
            if not event:
                # Retry.
                self._flush_events('button')
                self.progr = self.after(10, _capture, 'button', msgtxt)
            else:
                # Translate the "Event" to the Star Citizen naming.
                joy_name = self.joysticks[event.which][5]
                binding  = self.translator.translate_event(event, joy_name)
                input.set(binding)
                dialog.config(state = tk.NORMAL)
                done_button.state(['!disabled'])
                capture_button.state(['!disabled'])

        def _done(*args):
            """return the modifier and exit."""
            # Attempt to map the key.
            key = input.get()
            if self.mapping.set_modifier_key(key):
                # Create the "Modifier" frame.
                modifier_frame = ttk.LabelFrame(modifiers_frame, padding=5)

                # Define lambada function for del modifier.
                del_modifier = lambda target=modifier_frame, var=key: self._del_modifier_key(target, var)

                # Set the "Binding" label.
                key_label = ttk.Label(modifier_frame, text=key)

                # Create the "Delete Modifier" button.
                del_modifier_button = ttk.Button(modifier_frame, text=DELMOD, command=del_modifier)

                # Pack all items.
                modifier_frame.pack(side="bottom", fill="both", expand=True)
                key_label.pack(side="left", fill="both", expand=True)
                del_modifier_button.pack(side="left", fill="x")

            # Close the window.
            self.progr = self.after(0, _exit, msgtxt)

        def _exit(*args):
            """Exit without changes."""
            if self.progr: # Cancel any programmed command.
                self.after_cancel(self.progr)
            input_window.grab_release()
            input_window.destroy()

        def _test_entry(postval, button):
            """Callback Function to test if the given text is empty and disable the button if so, otherwise enable it."""
            if postval.isspace() or (' ' in postval) or not postval:
                button.state(['disabled'])
            else:
                button.state(['!disabled'])
            return True

        # Create the input window and lock interacting with the main window.
        input_window = tk.Toplevel(padx=5, pady=5)
        input_window.title('NEWMOD')
        input_window.resizable(width=False, height=False)
        input_window.protocol("WM_DELETE_WINDOW", _exit)
        input_window.bind("<Return>", _done)
        input_window.grab_set()

        input_frame = ttk.Frame(input_window)
        input_frame.pack(side="top", fill="both")
        tips_frame = ttk.Frame(input_window)
        tips_frame.pack(side="bottom", fill="both")

        # Create the message for the tips.
        ttk.Label(tips_frame, padding=5, justify="center", text=SETMOD).pack(side="top", expand=True)

        # Create the message for the window.
        msgtxt = tk.StringVar()
        msgtxt.set(DIRIMSG)
        message = ttk.Label(input_frame, padding=5, justify="center", textvariable=msgtxt)
        message.pack(side="top", fill="both")

        # Create a lambda for capture.
        cap_btn = lambda : [self._flush_events(), _capture()]

        # Create the "Capture" button.
        capture_button = ttk.Button(input_frame, text=CAPTURE, command=cap_btn)
        capture_button.pack(side="top", fill="x", expand=True)

        # Create an traceable var for input value.
        input = tk.StringVar()
        input.set('')

        # Create the "Done" button.
        done_button = ttk.Button(input_frame, text=DONE, command=_done)

        # Create the "Cancel" button.
        cancel_button = ttk.Button(input_frame, text=CANCEL, command=_exit)

        # Create the entry field, make it focused and selected.
        lambda_test = lambda t, b=done_button: _test_entry(t, b)
        ItsNoSpace = self.register(lambda_test)
        dialog = tk.Entry(input_frame, textvariable=input, justify="center", bd=3, width=-35, validate='all', validatecommand=(ItsNoSpace, '%P'))
        dialog.pack(side="top", fill="x", expand=True)
        dialog.select_range(0, tk.END)
        dialog.focus_set()

        # Pack buttons.
        done_button.pack(side="left", fill="x", expand=True)
        cancel_button.pack(side="right", fill="x", expand=True)

        # Prepare capture.
        self._flush_events()

    def _del_modifier_key(self, modifier_fr, bind_value):
        """Delete a modifier key definition."""
        self.mapping.del_modifier_key(bind_value)
        modifier_fr.destroy()

    # File Functions.
    def _load(self):
        """Load a saved Layout."""
        name = FDialog.askopenfilename(filetypes=[('all files', '.*'), ('XML files', '.xml')],
                                      defaultextension='.xml')
        if name:
            try:
                with open(name,'r') as file:
                    try:
                        devicedict, optiondict, keymapdict, header, modifiers = self._lay2dicts(file)
                    except Exception as Exc:
                        MBox.showerror(ERROR, "{0}:\n{1}".format(ERRXML, Exc))
                    else:
                        self.mapping.set_modifiers(modifiers)
                        self.mapping.set_dev(devicedict)
                        self.mapping.set_opt(optiondict)
                        self.mapping.set_map(keymapdict)
                        self.mapping.set_head(header)
                        self.header_var.set(header)
                        self._reset_edit_widget()
                        self._reset_joys_widget()
                        self._reset_head_widget()
                        self._update_text(self.mapping.get_text(), self.vmap_text)
                        self._update_text(self.mapping.get_xml(), self.vxml_text)
            except Exception as Exc:
                MBox.showerror(ERROR, "{0}:\n{1}".format(ERROPEN, Exc))

    def _store(self):
        """Save actual Layout."""
        name = FDialog.asksaveasfilename(filetypes=[('all files', '.*'), ('XML files', '.xml')],
                                        defaultextension='.xml')
        if name:
            try:
                with open(name,'w') as file:
                    file.write(self.mapping.get_xml())
            except Exception as Exc:
                MBox.showerror(ERROR, "{0}:\n{1}".format(ERROPEN, Exc))

    def _summ(self):
        """Save actual Layout summary."""
        name = FDialog.asksaveasfilename(filetypes=[('all files', '.*'), ('TXT files', '.txt')],
                                        defaultextension='.txt')
        if name:
            try:
                with open(name,'w') as file:
                    file.write(self.mapping.get_text())
            except Exception as Exc:
                MBox.showerror(ERROR, "{0}:\n{1}".format(ERROPEN, Exc))

    # Event capture Functions.
    def _capture_axis(self):
        """Capture an "Axis Event" if any."""
        event = sdl2.SDL_Event()
        got = sdl2.SDL_PollEvent(event)
        if got == 1:
            if event.type == ATTAXIS and event.jaxis.which not in self.ignorelist:
                if abs(event.jaxis.value) < 26000:
                    _log_data('    Event Wobbly:   {0}'.format(event_discover(event)))
                else:
                    _log_data('    Event Accept: {0}'.format(event_discover(event)))
                    return event.jaxis
            else:
                _log_data('    Event Reject: {0}'.format(event_discover(event)))

    def _capture_button(self):
        """Capture a "Button Event" if any."""
        event = sdl2.SDL_Event()
        got = sdl2.SDL_PollEvent(event)
        if got == 1:
            if   event.type == ATTBTTN and event.jbutton.which not in self.ignorelist:
                _log_data('    Event Accept: {0}'.format(event_discover(event)))
                return event.jbutton
            elif event.type == ATTHATS and event.jhat.which not in self.ignorelist:
                if event.jhat.value in [sdl2.SDL_HAT_UP, sdl2.SDL_HAT_RIGHT, sdl2.SDL_HAT_DOWN, sdl2.SDL_HAT_LEFT]:
                    _log_data('    Event Accept: {0}'.format(event_discover(event)))
                    return event.jhat
                else:
                    _log_data('    Event Hat Discard: {0}'.format(event_discover(event)))
            else:
                _log_data('    Event Reject: {0}'.format(event_discover(event)))

    def _capture_something(self):
        """Capture a "Hat Event" if any."""
        event = sdl2.SDL_Event()
        got = sdl2.SDL_PollEvent(event)
        if got == 1:
            if   event.type == ATTBTTN and event.jbutton.which not in self.ignorelist:
                _log_data('    Event Accept: {0}'.format(event_discover(event)))
                return event.jbutton
            elif event.type == ATTHATS and event.jhat.which not in self.ignorelist:
                if event.jhat.value in [sdl2.SDL_HAT_UP, sdl2.SDL_HAT_RIGHT, sdl2.SDL_HAT_DOWN, sdl2.SDL_HAT_LEFT]:
                    _log_data('    Event Accept: {0}'.format(event_discover(event)))
                    return event.jhat
                else:
                    _log_data('    Event HatUnk: {0}'.format(event_discover(event)))
            elif event.type == ATTAXIS and event.jaxis.which not in self.ignorelist:
                if abs(event.jaxis.value) < 26000:
                    _log_data('    Event Wobbly:   {0}'.format(event_discover(event)))
                else:
                    _log_data('    Event Accept: {0}'.format(event_discover(event)))
                    return event.jaxis
            else:
                _log_data('    Event Reject: {0}'.format(event_discover(event)))

    # Misc Functions.
    def _update_text(self, text, target):
        """Update the text on the given target "tk.Text" widget."""
        target.config(state = tk.NORMAL)    # Enable writing.
        target.delete('1.0', tk.END)        # Empty the widget.
        target.insert(tk.END, text)         # Text insert.
        target.config(state = tk.DISABLED)  # Disable writing.

    def _flush_events(self, keep=None):
        """Clear all Events from queue."""
        sdl2.SDL_PumpEvents()
        if keep != 'axis':
            sdl2.SDL_FlushEvent(ATTAXIS)
        if keep != 'button':
            sdl2.SDL_FlushEvent(ATTBTTN)
            sdl2.SDL_FlushEvent(ATTHATS)

    def _lay2dicts(self, xml):
        """Read the actionmap rebind file and return a tuple of Python dict."""
        dev_dic = OrderedDict()
        opt_dic = OrderedDict()
        map_dic = OrderedDict()
        modifiers = set()
        header  = ""
        dz_x  = 0.015
        dz_y  = 0.015
        dz_z  = 0.015
        dz_rx = 0.015
        dz_ry = 0.015
        dz_rz = 0.015
        dz_s1 = 0.015
        dz_s2 = 0.015
        for line in xml.readlines():
            if   '<actionmap' in line:
                actionmap_name = line.split('name="')[1].split('"')[0]
                map_dic[actionmap_name] = OrderedDict()
            elif '<action' in line:
                action_name = line.split('name="')[1].split('"')[0]
                map_dic[actionmap_name][action_name] = {}
                map_dic[actionmap_name][action_name]['device'] = "unknown"
                map_dic[actionmap_name][action_name]['input'] = {0:" "}
                index = 1
            elif '<CustomisationUIHeader' in line:
                header = line.split('label="')[1].split('"')[0]
            elif '<rebind' in line:
                device = line.split('device="')[1].split('"')[0]
                input  = line.split('input="')[1].split('"')[0]
                map_dic[actionmap_name][action_name]['device'] = device
                map_dic[actionmap_name][action_name]['input'][0] = input
            elif '<addbind' in line:
                device = line.split('device="')[1].split('"')[0]
                input  = line.split('input="')[1].split('"')[0]
                map_dic[actionmap_name][action_name]['input'][index] = input
                index += 1
            elif '<deviceoptions' in line:
                device_name = line.split('name="')[1].split('"')[0]
            elif '<option input="x"' in line:
                dz_x = float(line.split('deadzone="')[1].split('"')[0])
            elif '<option input="y"' in line:
                dz_y = float(line.split('deadzone="')[1].split('"')[0])
            elif '<option input="z"' in line:
                dz_z = float(line.split('deadzone="')[1].split('"')[0])
            elif '<option input="throttlez"' in line:
                dz_z = float(line.split('deadzone="')[1].split('"')[0])
            elif '<option input="rotx"' in line:
                dz_rx = float(line.split('deadzone="')[1].split('"')[0])
            elif '<option input="roty"' in line:
                dz_ry = float(line.split('deadzone="')[1].split('"')[0])
            elif '<option input="rotz"' in line:
                dz_rz = float(line.split('deadzone="')[1].split('"')[0])
            elif '<option input="slider1"' in line:
                dz_s1 = float(line.split('deadzone="')[1].split('"')[0])
            elif '<option input="slider2"' in line:
                dz_s2 = float(line.split('deadzone="')[1].split('"')[0])
            elif '</deviceoptions>' in line:
                dev_dic[device_name] = (dz_x, dz_y, dz_z, dz_rx, dz_ry, dz_rz, dz_s1, dz_s2)
            elif '<options type="joystick"' in line:
                joy = line.split('instance="')[1].split('"')[0]
                curves = {}
            elif '<flight' in line:
                curve_name      = line.split('<')[1].split(' ')[0]
                try:
                    invert      = int(line.split('invert="')[1].split('"')[0])
                except:
                    invert      = 0
                try:
                    sensitivity = float(line.split('sensitivity="')[1].split('"')[0])
                except:
                    sensitivity = 1.0
                try:
                    exponent    = float(line.split('exponent="')[1].split('"')[0])
                except:
                    exponent    = 1.0
                curve = {'invert':invert, 'sense':sensitivity, 'exponent':exponent, 'data':{0.0:0.0, 1.0:1.0}}
                if '/>' in line:
                    curve['data'] = {0.0:0.0, 0.25:0.25, 0.5:0.5, 0.75:0.75, 1.:1.}
                    curves[curve_name] = curve
            elif '<point' in line:
                x_point = float(line.split('in="')[1].split('"')[0])
                y_point = float(line.split('out="')[1].split('"')[0])
                curve['data'][x_point] = y_point
            elif '</flight' in line:
                curves[curve_name] = curve
            elif '</options>' in line:
                opt_dic[joy] = curves
            elif '<add input=' in line:
                modifiers.add(line.split('input="')[1].split('"')[0])
        return dev_dic, opt_dic, map_dic, header, modifiers

    def tryfloatval(self, postval, min, max):
        """Callback Function to check entry fields."""
        try:
            val = float(postval)
        except:
            if postval == '':
                return True
            else:
                return False
        else:
            if float(min) <= val <= float(max):
                return True
            else:
                return False

def _log_data(data):
    if LOGDATA:
        if LOGFILE:
            with open(LOGFILE, 'a') as file:
                file.write('{0} >> {1}\n'.format(time.asctime(), data))
        if LOGCONS:
            print('{0} >> {1}'.format(time.asctime(), data))

def event_discover(event):
    return str(EVENTDICT.get(event.type,event.type))

def list_uniques(array):
    """Return a list with duplicates removed."""
    returned = []
    for item in array:
        if item not in returned:
            returned.append(item)
    return returned

def MAIN():
    """Main module function."""
    global HOMETEXT
    HOMETEXT = HOMETEXT.replace('$$VERSION$$', VERSION)
    HOMETEXT = HOMETEXT.replace('$$YEAR$$', YEAR)

    print('Running on: Python {0}'.format(sys.version))
    print(WELLMSG)
    print('')
    print(HOMETEXT)
    _log_data('######################### New Sesion. #########################')

    sdl2.SDL_Init(sdl2.SDL_INIT_JOYSTICK)
    mapping     = Mapping(ACTIONMAP)
    translator  = Translator(AXESDICT)
    tkapp       = Config_App(mapping, translator)
    tkapp.start()
    # Program will stop here until the Tk application is closed.

def EXIT():
    """Clean "Exit" module function."""
    print(EXITMSG)
    _log_data('######################### End Sesion. #########################')
    sdl2.SDL_Quit()

if __name__ == "__main__":
    if LANGUAGE:
        MAIN()
        EXIT()
