REM Make_script for PyMakexe.

REM Clear Previous Dist.
rmdir /S /Q dist

REM Make the project.
python -OO C:\Python27\Tools\Scripts\pymakexe.py -e numpy -z lib/shared.zip -o 2 -c -wo "JoyConfig4SC.py" -co "Profile2Layout.py" "Layout2Dic.py" "Layout2OrdDic.py"

REM Remove Build data.
rmdir /S /Q build

REM Copy additional data to Dist directory.
cd dist
mkdir data
mkdir "profiles&layouts"
cd ..
copy "*.txt" "dist"
xcopy "data" "dist/data" /E /Y
xcopy "lib"  "dist/lib"  /E /Y
xcopy "profiles&layouts\*" "dist/profiles&layouts" /E /Y
ren dist JoyConfig4SC
mkdir dist
move JoyConfig4SC dist\JoyConfig4SC