﻿JoyConfig4SC V 4.5
Copyright (c) 2014, Carlos Bazaga.
Licensed under BSD 2-Clause.
All rights reserved.

Changelog:

    V. 4.5                  (xx-yyy-2015) 
    
            ********** TO BE RELEASED **********
            
        •Updated for SC 2.2.
    
        •Updated Python underlaying interpreter to 2.7.10

        •Fixed example files.
        •Fixed some typos.
        •Minor bugfixes.



    V. 4.4                  (30-Aug-2015)
        •Revision number update to note new AC V1.2 compatibility.
         No need to update unless you want to track version numbering.

    V. 4.3                  (28-Aug-2015)
        •Updated action lists to AC V1.1.6a Actionmaps:
            ·Added following actions:
                skyLine_toggle:                 Not sure, but it seems related to star travel. (Not yet in game)
                v_comm_open_team_chat:          Turns on/off team chat line.
                mobiglas:                       It should open the mobiglas interface. (Not yet in game)

            ·Modified following actions:
                v_decoupled_pitch_up            (added joy support)
                v_decoupled_pitch_down          (added joy support)

        •New curve settings:
            ·Added curve settings:
                turret_aim_pitch                Vertical movement of turrets.
                turret_aim_yaw                  Lateral movement of turrets.

        •Headers now will use default description and image from AC instead of Saitek's X52 ones.
            ·Still encourage to load layouts via console and "pp_rebindkeys" command, so no header should be necessary.

        •Added limited experimental support for XBoxController:
            ·Now there are axes and button names mapping to XBoxController device.

            ·Only bindings and curves are supported at this moment, no deadzones.

        •Modifier keys declaration and assignment:
            ·The "Header" tab has been extended to define keys that are going to be used as modifier keys.

             In the new AC version keys that should be used as modifiers must be declared first.

                The XboxPad buttons:
                    xi_shoulderl, xi_shoulderr, xi_triggerl_btn, xi_triggerr_btn, xi_shoulderr, xi_back
                are defaulted so no need to re add them.

             Keys and buttons to be used as modifiers can be setted directly or capturing joystick events,
             just the same way as in the "Edit" tab.

            ·Added a new functionality on the "Edit" tab to add modifiers to an actual bind.

             Adding a modifier works the same as defining them, just write it or use the "capture"
             option to make the application detect a joystick event.

             In order to a modifier to be acceptable it must be defined first on the "header" section.

             To get rid of modifiers just clear the entire binding line or edit it on the "Direct input" option.

        •Updated profiles&layouts.
        •Updated default values and action devices.
        •Updated action descriptions.
        •Updated "Direct Input" help text.

        •Some translations completed
        •Fixed layout loading when "options" setted with no curve points.
        •Fixed example files.
        •Fixed some typos.
        •Minor bugfixes.

    V. 4.2                  (16-May-2015)
        •Updated action lists to AC V1.1.3 Actionmaps:
            ·Added following actions:
                v_ifcs_toggle_esp:              Turns on/off ESP, an AI assistant who manages sensibility.
                v_target_cycle_reticle_mode:    Switches between lag-pis and lead-indicators.
                v_turret_aim_pitch:             Not sure, but it seems related to turret aiming.
                v_turret_aim_yaw:               Not sure, but it seems related to turret aiming.

        •Direct Input now defaults to the actual binding:
            ·This is done to ease modifier keys addition, this way to add a modifier key just add it before
             the binding with a + symbol between them:

                    Example:
                                lctrl+js1_button1

            ·Only "lctrl" and "rctrl" keys are supported as modifiers by the game at this moment.
            ·In case CIG implements a more developed "modifier keys" feature I'll include a specific
             functionality to add them to the layouts.

        •Updated profiles&layouts.
            ·Including the null_layout.xml to be loaded after a "bindings wipe" by "pp_rebindkeys" command alone.

        •Updated default values and action devices.
        •Updated action descriptions.
        •Updated "Direct Input" help text.

        •Fixed example files.
        •Fixed some typos.
        •Minor bugfixes.


    V. 4.1                  (04-April-2015)
        •Updated action lists to AC V1.1 Actionmaps:
            ·Landing controls expanded and moved from "spaceship_general" to "spaceship_movement".
            ·Power controls expanded and moved from "spaceship_general" to "spaceship_power".
            ·Removed "v_hud_toggle_cursor_input".

        •New curve settings:
            ·Added curve settings for "Aim", "View", "Zoom" and "Throttle" axes.
            ·Now all game curve settings are available
            ·NOTE: "flight_throttle_abs" and "flight_move_strafe_longitudinal" invert defaults to '1'.
             Thats due to Throttle standard construction, full back means 100% of signal.

        •New Deadzones settings:
            ·Added support for setting deadzones for every device axis.

             NOTE: Axis deadzones are set for device kind, not in game axis.

                 That means setting a deadzone of '0.5' for "x" axis on device
                 "ExampleJoyName" will not affect the "x" axis of "OtherJoyName".

                 Also, if you have more than one "ExampleJoyName" installed
                 they all will share the '0.5' deadzone value.

                 This is based on how the game handle deadzones and
                 there is no chance for the user to alter that.

        •Added functionality to create a Header for in game control settings UI.
            ·This will create an entry on the in game advanced control settings menu.
             As the in game interface for loading layouts is completely unintuitive I'm not sure it works well.
             It seems there is an annoying table to remap devices which makes no sense at all.

            ·I encourage to load layouts via console and "pp_rebindkeys" command.

        •Added a functionality to ignore certain joysticks while capturing inputs.
            ·You can set the joystick to get ignored with the button in the joystick tab.
             That will make the joystick to be ignored while capturing events on "Edit Layout" window.
             Use it to avoid any device interfering with the proper capture of your inputs.

        •Fixed example files.
        •Fixed some typos.
        •Minor bugfixes.


    V. 4.0                  (31-January-2015)
        •Added an option to bind more than one button/axis to the same action.
            ·General Edit Layout GUI reworked to support this new functionality.
                -Each action now has a button to add new bind lines, each line
                 can hold a different binding for that action.
                -Lines can be removed using the button next to them, except the
                 initial one.

            ·Disabled option to capture joystick events for Keyboard bound actions.
                -In those cases you should add a new bind line which
                 will be always joystick bound.

                WARNING!
                -For bind lines with the "capture" button disabled only keyboard or
                 mouse inputs should be bonded by the "direct input" option.
                -For bind lines with the "capture" button available only joystick
                 inputs should be bonded by the "direct input" option.

            ·The additional lines makes use of the "addbind" command.
                -Bindings added by the "addbind" command does not erase upon loading
                 new layouts, so to be sure they are wiped out call for
                 pp_rebindkeys (without arguments) before loading your own profile.

                 That way you can be sure your layout is being loaded over the default.

                 If you makes use of "null_layouts" to clear, load them after the wipe
                 and before your own layout.

        •Reviewed default device binds.
            · Loading and old layout will work, but it will take the devices from it.
              To be sure devices are correctly recognized and the layout generated is OK
              is recommended to cross check devices with the default of the app, or make
              a new layout from scratch.

        •Added French translation.
        •Fixed example files.
        •Fixed some typos.
        •Minor bugfixes.

    V. 3.5                  (29-December-2014)
        •Added all new curve settings up to total of six.
        •Some GUI fixes:
            ·Added a slider on curve settings to avoid an excessive wide window.
            ·Actions subdivided in group tabs to avoid an excessive wide window.
        •Reviewed bindable actions:
            ·On AC V1.0 ability to set joystick functions over keyboard only actions is lost.
            ·On edit tabs each action name comes tagged for keyboard or joystick bound
             and while the app will allow to bind joystick actions to keyboard bound actions
             the game will not recognize them.
             So stick to keyboard bindings on keyboard bound actions and joystick bindings on
             joystick bound actions until CIG offers a solution for this inconvenience.
             (Warning: Examples given may contain errors of this kind.)
        •Multiple binds for the same action are not already supported:
            ·While game supported, they are not available into the app.
             They will come in a future release with proper GUI changes.
        •Some updates and fixes on translations.
        •Minor bugfixes.

    V. 3.4                  (10-December-2014)
        •Added an option to invert axial actions
        •Added German translation.
        •Fixed some typos in English.
        •Added curve point definitions for 0 and 1 on the generated XML.
        •Minor bugfixes.

    V. 3.3                  (11-November-2014)
        •New support for previous versions layouts:
            ·When loading a layout new bindings are added and deprecated ones removed.
            ·Verbose load, shows in console binding changes and deprecated removals.
             To view new bindings just compare your previous layout with the new base one.
            ·It will update device bonded to action too.
            ·Need to "Save Layout" to update the file.
            ·Not auto-remove of deprecated curve options, has to be done manually.
             You'll need to use "clear options" button on the proper joystick tab,
             this removes them all from the xml but right ones keeps in the joystick tab
             to be edited,so hitting "set options" will set just the right ones.
        •New event capture code:
            ·Now the app requires axes to be moved at 80% of its range to be detected.
            ·Avoids event loses.
            ·More stable and faster capture.
            ·Avoids "jittering" axes to interfere.
        •Non Event Verbose build, also not generates event_log.txt file.
            ·Verbose was added to track event loses and jittering captures.
            ·Still coded but disabled.
             (In future versions this would be enabled inside the app at users command.)
        •Minor bugfixes.

    V. 3.2                  (07-November-2014)
        •Updated to AC 0.9.2 new Flight/Aim Modes.
        •Minor bugfixes.

    V. 3.1                  (03-October-2014)
        •Updated to AC 0.9.1 new Action Bindings.
        •Updated to AC 0.9.1 new Axes Tweaking.
        •Minor bugfixes.
        •Improved axis tweaking code.
        •Updated example profiles and layouts with curve tweaks.
        •Translated to the four languages.
            (Google Translator for Russian and Italian)

    V. 3.0                  (30-September-2014)
        •Added tools for axes curves tweaking.
        •Save and Load layouts with curves.
        •Minor bugfixes.
        •Curve configuration only in English.
            (By now, translation will come in a future update)
        •Updated example profiles and layouts with curve tweaks.
        •Coded new module for Graphical representation of curves.
        •Thanks to collaborators who helped to make this big update faster:
            ·Thanks to SkyZone for his:
                "Dealing with non-linear multi-point curves tutorial"
                You may retrieve a copy at:
                https://dl.dropboxusercontent.com/u/13355037/Multi%20point%20curves-1.pdf
            ·Thanks to Xylker for curve and graph maths testing and comments.

    V. 2.0                  (23-September-2014)
        •New AC 0.9 release.
        •Minor bugfixes.
        •Updated to AC 0.9 new Action Bindings.
        •Updated example profiles and layouts.

    V. 1.1                  (6-August-2014)
        •Minor bugfixes.
        •Added Russian translation.
        •Reworked axes names translation switch.
        •Added naming preset for "Thrustmaster T Hotas X". (Thanks to Cuckoo)
        •Verbose version, prints logs on console. It also now
         generates a event_log file for tracking event capture bugs.
        •Added a list of standard codes to the "Direct Input" form.

    V 1.0                   (10-July-2014)
        •Initial release.

How to use JoyConfig4SC and load a profile in Star Citizen:
    Unzip the package in your preferred directory and run JoyConfig4SC.exe
    to start the application.

    Edit and save your layout (You'll find a manual inside the application),
    we will assume it's saved as "your_own_profile.xml"
    save it in StarCitienDir\CitizenClient\Data\Controls\Mappings.

    Once you has saved your layout copy there, in the same directory,
    the "null_layout.xml" file found in JoyConfg4SC\Profiles&Layouts
    and then run the game.

    Once in the game open the interactive in-game console.
        (Leftmost key in the numbers row)
    and write this three commands in order:

        pp_rebindkeys                   (Will load default bindings)
        pp_rebindkeys null_layout       (Will set all joystick bindings to a "null" value)
        pp_rebindkeys your_own_layout   (Will load your own bindings)

    All it's done, enjoy killing Vanduuls :-D

Zipped Package contents:
\
  README.txt            : This file.
  LICENSE.txt           : BSD 2-Clause license.
  JoyConfig4SC.exe      : Configurator application. (Manual inside)
  Layout2Dic.exe        : Command-line application, translates a Rebinds Layout
                            into a dict structure.

          Usage: Layout2Dict Input_layout_file.xml Output_dict_file.py

          No actual use by now. You can ignore and even delete this file.

  Layout2OrdDic.exe     : Command-line application, translates a Rebinds Layout
                            into an OrderedDict structure.

          Usage: Layout2OrdDict Input_layout_file.xml Output_dict_file.py

          Used in combination with "Profile2Layout" to update default application data
          in case of a game update changes actio_names, default_profile, etc.

          You do not need to use this at all just to use JoyConfig4SC.
          For standard usage you can ignore and even delete this file.

  Profile2Layout.exe    : Command-line application, translates a SC ActionMaps profile
                            into a Rebinds Layout.

          Usage: Profile2Layout Input_profile_file.xml Output_layout_file.xml

          Used in combination with "Layout2OrdDic" to update default application data
          in case of a game update changes actio_names, default_profile, etc.

          You do not need to use this at all just to use JoyConfig4SC.
          For standard usage you can ignore and even delete this file.

  python27.dll                  : Python's DLL file.

  \data                         : Collection of basa data naming and localization.
    Actions.dict                : Default Layout Ordered Dictionary.
    Axes.dict                   : Default Axes translation Sheets.
    Languages.dict              : Dictionary for available languages.
    Modes.dict                  : Default control input modes (Axis/Button) Dictionary.
    XBox.dict                   : XBox Axes translation Dictionary.

    \{Lang}                     : A directory for each language translation files.
      Descriptions.dict         : Actions descriptions Dictionary.
      Texts.dict                : General text of Homepage.
      Words.dict                : Dictionary of terms.

  \lib                          : Application resources directory.
    README-SDL.txt              : SDL2 redistribution text.
    SDL2.dll                    : SDL2 redistributable library.
    ...
    \tcl                        : Tcl\Tk Resources directory.
        ...

  \profiles&layouts             : A short collection of Profiles and Layouts.
    base_layout.xml             : Extracted Layout for base_profile.
    base_profile.xml            : A base profile for Joystick usage.
    default_layout.xml          : Extracted Layout for default_profile.
    default_profile.xml         : StarCitizen DefaulProfile.
    null_layout.xml             : Extracted Layout for null_profile.
                                    Load in game just before loading a personalized layout,
                                    that will prevent overlapping joysticks bindings.

    null_profile.xml            : A null profile for joystick input.
                                   (It sets all joystick inputs to "jsx_reserved")

    \Examples                   : A short collection of examples.
      Example_X52_layout.xml    : An example layout for Saitek's X52 (Actually my own layout)
      Example_X52_map.txt       : Textual mapping for the example layout.
      ...

SouceCode at: https://bitbucket.org/CarBaz/joyconfig4sc

Thanks to all contributors, testers and translators.

Disclaimer:
    This program is independent of Cloud Imperium Games
        and is not representative of their work.
    Its provided for the community to help writing controller .xml configuration files.
    Star Citizen names and content are Copyright © Cloud Imperium Games.