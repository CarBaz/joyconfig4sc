#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#
from sys import argv
"""
Expects an actionmap rebind Layout as first argument.
It will read the actionmap rebind file and write out a Python OrderedDict on second argument.
"""
def DoIt(xml, dic):
    dic.write('ACTIONMAP = [\n')
    for line in xml.readlines():
        if   '<actionmap' in line:
            name = line.split('name="')[1].split('"')[0]
            dic.write("    ('{0}', [\n".format(name))
        elif   '</actionmap' in line:
            dic.write('    ]),\n')
        elif '<action' in line:
            name   = line.split('name="')[1].split('"')[0]
            dic.write("                                ('{0}', [".format(name))
        elif '<rebind' in line:
            device = line.split('device="')[1].split('"')[0]
            input  = line.split('input="')[1].split('"')[0]
            dic.write("('device', '{0}'), ('input', {{0:'{1}'}})]),\n".format(device, input))
    dic.write(']\n')

if __name__ == "__main__":
    if len(argv) != 3:
        print("Usage: Layout2OrdDict Input_layout_file.xml Output_dict_file.py")
    elif raw_input('This will erase {0}, input "yes" to continue: '.format(argv[2])) == 'yes':
        with open(argv[1],'r') as xml:
            with open(argv[2],'w') as dic:
                DoIt(xml, dic)
    print("Bye")
