#
# Retrieved from: http://tkinter.unpythonic.net/wiki/VerticalScrolledFrame
#
# Modified by: Carlos Bazaga
#   Exposed 'canvas' attribute to allow for direct scrolling.
#
import Tkinter as tk
import ttk

class VerticalScrolledFrame(ttk.Frame):
    """
    A pure Tkinter scrollable frame.

    Use the 'interior' attribute to place widgets inside the scrollable frame.
    Construct and pack/place/grid normally.
    This frame only allows vertical scrolling.

    Mods:
        Use the 'canvas' attribute to handle direct scroll.
            IE:
                    # If the required height of the 'interior' is greater
                    # than the allowed space in 'canvas' scrolling is required.

                    if my_vscrolledframe.interior.winfo_reqheight() > my_vscrolledframe.canvas.winfo_height():
                        my_vscrolledframe.canvas.yview_scroll(-1*(event.delta/120), "units")
    """

    def __init__(self, parent, *args, **kw):
        ttk.Frame.__init__(self, parent, *args, **kw)

        # create a canvas object and a vertical scrollbar for scrolling it
        vscrollbar = ttk.Scrollbar(self, orient="vertical")
        vscrollbar.pack(fill="y", side="right", expand=False)
        self.canvas = canvas = tk.Canvas(self, bd=0, highlightthickness=0, yscrollcommand=vscrollbar.set)
        self.canvas.pack(side="left", fill="both", expand=True)
        vscrollbar.config(command=self.canvas.yview)

        # reset the view
        canvas.xview_moveto(0)
        canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = interior = ttk.Frame(canvas)
        interior_id = canvas.create_window(0, 0, window=interior, anchor="nw")

        # track changes to the canvas and frame width and sync them,
        # also updating the scrollbar
        def _configure_interior(event):
            # update the scrollbars to match the size of the inner frame
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the canvas's width to fit the inner frame
                canvas.config(width=interior.winfo_reqwidth())
        interior.bind('<Configure>', _configure_interior)

        def _configure_canvas(event):
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the inner frame's width to fill the canvas
                canvas.itemconfigure(interior_id, width=canvas.winfo_width())
        canvas.bind('<Configure>', _configure_canvas)