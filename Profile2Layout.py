#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#
from sys import argv
"""
Expects an actionmap Profile as first argument.
It will read the actionmap file and write out a rebind Layout on second argument.
"""
def DoIt(pro, lay):
    lay.write('<ActionMaps ignoreVersion="1" >\n')
    for line in pro.readlines():
        if   '<actionmap ' in line:
            name = line.split('name="')[1].split('"')[0]
            lay.write('    <actionmap name="{0}">\n'.format(name))
        elif   '</actionmap' in line:
            lay.write('    </actionmap>\n')
        elif '<action ' in line:
            name = line.split('name="')[1].split('"')[0]
            if "joystick=" in line:
                input = line.split('joystick="')[1].split('"')[0]
                if input == "":
                    if "keyboard=" in line:
                        device = "keyboard"
                        input  = line.split('keyboard="')[1].split('"')[0]
                    else:
                        continue
                else:
                    device = "joystick"
            elif "keyboard=" in line:
                device = "keyboard"
                input  = line.split('keyboard="')[1].split('"')[0]
            else:
                continue
            lay.write('        <action name="{0}">\n'.format(name))
            lay.write('            <rebind device="{0}" input="{1}"/>\n'.format(device, input))
            lay.write('        </action>\n')
    lay.write('</ActionMaps>\n')

if __name__ == "__main__":
    if len(argv) != 3:
        print("Usage: Profile2Layout Input_profile_file.xml Output_layout_file.xml")
    elif raw_input('This will erase {0}, input "yes" to continue: '.format(argv[2])) == 'yes':
        with open(argv[1],'r') as pro:
            with open(argv[2],'w') as lay:
                DoIt(pro, lay)
    print("Bye")
